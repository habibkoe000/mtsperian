<?php


session_start();
include_once('../konek/coonnect.php');
include_once('../query/queryClass.php');

$dC = new queryClass;
$siswa_kelas = $dC->siswa_kelas();
$pegawai = $dC->pegawai();
$tahun_ajaran = $dC->tahun_ajaran();
$tingkat = $dC->tingkat();
$k_b = $dC->kat_b();
$agama = $dC->agama();
$pendidikan = $dC->pendidikan_terahir();
$pekerjaan_wali = $dC->pekerjaan_wali();
$siswa = $dC->siswa();
$penghasilan = $dC->penghasilan_wali();
$guru = $dC->guru();
$f_k = $dC->filter_kata();
$fasilitas = $dC->fasilitas();
$profil = $dC->profil_sekolah();
$penelitian = $dC->penelitian();
$mapel = $dC->mapel();
$kegiatan = $dC->semua_kegiatan2();
$smester = $dC->smester();

if(isset($_SESSION['id_karyawan'])){
	$id_user = $_SESSION['id_karyawan'];
$pg = $_GET['page'];

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pg ; ?> > guru mts nw perian </title>
<link rel="shortcut icon" href="../gambar/favicon.png" />
<link rel="stylesheet" href="asset/style.css" type="text/css" />
<link rel="stylesheet" href="asset/menu-samping.css" type="text/css" />
<script src="../js/jquery2.1.js" type="text/javascript"></script>
<script type="text/javascript" src="../tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "textarea#isi_body",
    theme: "modern",
    width: 995,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/Style.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 
</script>

</head>

<body>

<div class="header">
	<a href="?page=branda"><img src="gambar/logo.png" /></a>
    <div class="head_cari">
    	<form>
        	<input type="text" name="cari" placeholder="cari..." class="form-cari" />
        </form>
    </div>
    <div class="head-kanan">
    	<?php 
		global $pdo;
	$query = $pdo->prepare("SELECT * FROM login_guru WHERE id_karyawan = ?");
	$query->bindValue(1, $id_user);
	$query->execute();	
	$baris = $query->fetch();
		echo '<a href="?page=guru_data&tampil">';
		echo '<img src="gambar/setting.png" />';
		echo $baris['username']; 
		echo '</a>';
		?>
    </div>
    <div class="head_out">
    	<ul class="menu">
    	<li class="item9"><a href="logout.php">Logout</a>
        </li>
        </ul>
	</div>
</div>
	<div class="side-menu-kiri">
    	<?php include('template/sidebar-left.php'); ?>
    </div>
    <?php if($pg =='branda') { 
		include('kontent/branda-awal.php');	
	 } else if($pg == 'guru_data'){
		 include('kontent/guru_data.php');
	 }else if($pg == 'pengumuman'){
		 include('kontent/pengumuman.php');
	 }else if($pg == 'nilai'){
		 include('kontent/nilai.php');
	 }else if($pg == 'absensi'){
		 include('kontent/absensi.php');
	 }else if($pg == 'nilai_lihat'){
		 include('kontent/nilai_lihat.php');
	 }?>
</body>
</html>
<?php
}else { ?>
<script language="javascript">
	alert("Maaf, Anda Harus Login Dulu Untuk Mengakses Halaman Ini!!");
	document.location="login.php?page=login";
	</script>
	<?php } ?>