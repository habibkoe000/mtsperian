<?php
	class queryClass{
	
	public function berita_awal(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM berita WHERE kategori='berita_umum' ORDER BY id_berita DESC  LIMIT 0, 3");
		$query->execute();
		return $query->fetchAll();
	}
	public function penelitian(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM penelitian_guru ORDER BY id DESC  LIMIT 0, 3");
		$query->execute();
		return $query->fetchAll();
	}
	public function smester(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM smester ORDER BY id_smester DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function prestasi(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM prestasi ORDER BY id DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function hari(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM hari ORDER BY id_h ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function jam(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jam_belajar ORDER BY id_jam ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function jam_e($id_j){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jam_belajar WHERE id_jam = ?");
		$query->bindValue(1, $id_j);
		$query->execute();
		return $query->fetch();
	}
	public function agama_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM agama WHERE id = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function pendidikan_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pendidikan_terahir WHERE id = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function pekerjaan_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pekerjaan_wali WHERE id = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function penghasilan_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM penghasilan_wali WHERE id = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function ta_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM tahun_ajaran WHERE id_ta = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function hari_e($id_e){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM hari WHERE id_h = ?");
		$query->bindValue(1, $id_e);
		$query->execute();
		return $query->fetch();
	}
	public function dakwah(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM dakwah_siswa ORDER BY id_dakwah DESC  LIMIT 1");
		$query->execute();
		return $query->fetchAll();
	}
	public function kat_b(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM kategori_berita");
		$query->execute();
		return $query->fetchAll();
	}
	public function mapel(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM mata_pelajaran LEFT JOIN karyawan ON karyawan.id_karyawan=mata_pelajaran.id_karyawan");
		$query->execute();
		return $query->fetchAll();
	}
	public function mapel2($kelas){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM mata_pelajaran LEFT JOIN karyawan ON karyawan.id_karyawan=mata_pelajaran.id_karyawan WHERE mata_pelajaran.id_tingkat=?");
		$query->bindValue(1, $kelas);
		$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar WHERE ruangan=? and id_smester=? and tahun=?");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetch();
	}
	public function jadwal_belajar2($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='senin'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar3($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='selasa'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar4($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='rabu'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar5($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='kamis'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar6($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='sabtu'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function jadwal_belajar7($ruang,$sm,$ta){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM jadwal_belajar LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp=jadwal_belajar.id_mp WHERE ruangan=? and id_smester=? and tahun=? and hari='ahad'");
		$query->bindValue(1, $ruang);$query->bindValue(2, $sm);$query->bindValue(3, $ta);$query->execute();
		return $query->fetchAll();
	}
	public function mapel3($id_k){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM mata_pelajaran LEFT JOIN karyawan ON karyawan.id_karyawan=mata_pelajaran.id_karyawan WHERE mata_pelajaran.id_karyawan=?");
		$query->bindValue(1, $id_k);
		$query->execute();
		return $query->fetchAll();
	}
	public function mapel4($id_mp){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM mata_pelajaran WHERE id_mp=?");
		$query->bindValue(1, $id_mp);
		$query->execute();
		return $query->fetch();
	}
	public function filter_kata(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM filter_kata");
		$query->execute();
		return $query->fetchAll();
	}
	public function pekerjaan_wali(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pekerjaan_wali");
		$query->execute();
		return $query->fetchAll();
	}
	public function pendidikan_terahir(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pendidikan_terahir");
		$query->execute();
		return $query->fetchAll();
	}
	public function penghasilan_wali(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM penghasilan_wali");
		$query->execute();
		return $query->fetchAll();
	}
	public function agama(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM agama");
		$query->execute();
		return $query->fetchAll();
	}
	public function berita_awal_slide(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM berita  WHERE kategori='berita_slide' ORDER BY id_berita DESC LIMIT 0,3");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_berita($id_b){
		global $pdo;
		$query =$pdo->prepare("select * from berita where id_berita =?");
		$query->bindValue(1, $id_b);
		$query->execute();
		return $query->fetch();
		}
	public function detail_berita_tampil($id_b){
		global $pdo;
		$query =$pdo->prepare("select berita.foto, berita.tanggal, berita.id_berita, berita.isi_berita, berita.judul, karyawan.nama_lengkap, user.status from berita LEFT JOIN karyawan ON karyawan.id_karyawan = berita.id_penulis
		LEFT JOIN user ON user.id = berita.id_penulis where id_berita =?");
		$query->bindValue(1, $id_b);
		$query->execute();
		return $query->fetch();
		}
	
	public function pengumuman(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pengumuman ORDER BY id_pengumuman DESC LIMIT 2");
		$query->execute();
		return $query->fetchAll();
	}
	public function pengumuman_siswa(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pengumuman ORDER BY id_pengumuman DESC LIMIT 4");
		$query->execute();
		return $query->fetchAll();
	}
	public function semua_pengumuman(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pengumuman LEFT JOIN user ON user.id = pengumuman.penulis LEFT JOIN karyawan ON karyawan.id_karyawan = pengumuman.penulis ORDER BY id_pengumuman DESC");
		$query->execute();
		
		return $query->fetchAll();
	}
	public function semua_pengumuman_g($id_k){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM pengumuman LEFT JOIN karyawan ON karyawan.id_karyawan = pengumuman.penulis WHERE pengumuman.penulis=?");
		$query->bindValue(1, $id_k);
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_pengumuman($id_p){
		global $pdo;
		$query =$pdo->prepare("select * from pengumuman where id_pengumuman =?");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();	
	}
	
	public function kegiatan(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM kegiatan ORDER BY id_kegiatan DESC LIMIT 4");
		$query->execute();
		return $query->fetchAll();
	}
	public function semua_kegiatan(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM kegiatan ORDER BY id_kegiatan DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function semua_kegiatan2(){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM kegiatan LEFT JOIN user ON user.id=kegiatan.penulis ORDER BY id_kegiatan DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_kegiatan($id_k){
		global $pdo;
		$query =$pdo->prepare("select * from kegiatan where id_kegiatan =?");
		$query->bindValue(1, $id_k);
		$query->execute();
		return $query->fetch();
			
	}
	public function profil_sekolah(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM profil ORDER BY id ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_sekolah($id_p){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM profil where id =?");
		$query->bindValue(1, $id_p);
		$query->execute();
		return $query->fetch();
	}
	public function fasilitas(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM fasilitas ORDER BY id_fasilitas ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_fasilitas($id_f){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM fasilitas where id_fasilitas =?");
		$query->bindValue(1, $id_f);
		$query->execute();
		return $query->fetch();
	}
	public function pegawai(){
	global $pdo;
	$query = $pdo->prepare('SELECT * FROM karyawan ORDER BY nama_lengkap ASC');
	$query->execute();
	return $query->fetchAll();	
		
	}
	public function guru(){
	global $pdo;
	$query = $pdo->prepare("SELECT * FROM karyawan WHERE status_jabatan='Guru' ORDER BY nama_lengkap ASC");
	$query->execute();
	return $query->fetchAll();	
		
	}
	public function detail_guru($id_k){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM karyawan WHERE id_karyawan = ?");
		$query->bindValue(1, $id_k);
		$query->execute();
		return $query->fetch();
	}
	
	public function siswa_kelas(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas GROUP BY kelas.id_kelas ORDER BY kelas.id_kelas ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function siswa_kelas_detail($tingkat){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas WHERE tingkatan=?");
		$query->bindValue(1, $tingkat);
		$query->execute();
		return $query->fetchAll();
	}
	public function siswa_kelas_detail_1($tingkat){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas WHERE tingkatan=?");
		$query->bindValue(1, $tingkat);
		$query->execute();
		return $query->fetch();
	}
	public function siswa_kelas_detail2($n_r){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas WHERE nama_ruang=?");
		$query->bindValue(1, $n_r);
		$query->execute();
		return $query->fetchAll();
	}
	public function siswa_kelas_detail3($n_r){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas WHERE nama_ruang=?");
		$query->bindValue(1, $n_r);
		$query->execute();
		return $query->fetch();
	}
	public function detail_kelas($id_k){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM kelas WHERE id_kelas =?");
		$query->bindValue(1, $id_k);
		$query->execute();
		return $query->fetch();
	}
	public function detail_siswa_kelas($ruang_kel, $thun){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas LEFT JOIN data_siswa ON data_siswa.id_siswa=detail_siswa_kelas.id_siswa WHERE ruang =? and thn_ajaran=?");
		$query->bindValue(1, $ruang_kel);
		$query->bindValue(2, $thun);
		$query->execute();
		return $query->fetchAll();
	}
	public function nilai_uh($ruang, $thun){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM nilai_uh LEFT JOIN data_siswa ON data_siswa.id_siswa=nilai_uh.id_siswa WHERE kelas =? and thn_ajaran=?");
		$query->bindValue(1, $ruang);
		$query->bindValue(2, $thun);
		$query->execute();
		return $query->fetchAll();
	}
	public function nilai_pr($ruang, $thun){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM nilai_pr LEFT JOIN data_siswa ON data_siswa.id_siswa=nilai_pr.id_siswa WHERE kelas =? and thn_ajaran=?");
		$query->bindValue(1, $ruang);
		$query->bindValue(2, $thun);
		$query->execute();
		return $query->fetchAll();
	}
	public function nilai_us($ruang, $thun){
		global $pdo;	
		$query = $pdo->prepare("SELECT * FROM nilai_smester LEFT JOIN data_siswa ON data_siswa.id_siswa=nilai_us.id_siswa WHERE ruang_kelas =? and thn_ajaran=?");
		$query->bindValue(1, $ruang);
		$query->bindValue(2, $thun);
		$query->execute();
		return $query->fetchAll();
	}
	public function nilai_us2($ruang_kel, $thun, $id_s){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM nilai_smester LEFT JOIN mata_pelajaran ON mata_pelajaran.id_mp= nilai_smester.id_mp WHERE ruang_kelas =? and thn_ajaran=? and id_siswa=?");
		$query->bindValue(1, $ruang_kel);$query->bindValue(2, $thun);$query->bindValue(3, $id_s);$query->execute();
		return $query->fetchAll();
	}
	public function d_s_k($thn){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas WHERE thn_ajaran =?");
		$query->bindValue(1, $thn);
		$query->execute();
		return $query->fetchAll();
	}
	public function d_g_k($ruang, $thn){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas WHERE ruang=? and thn_ajaran=?  LIMIT 1");
		$query->bindValue(1, $ruang);
		$query->bindValue(2, $thn);
		$query->execute();
		return $query->fetch();
	}
	public function d_g_k2($ruang_kel, $tahun){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas LEFT JOIN karyawan ON karyawan.id_karyawan = detail_siswa_kelas.id_wali WHERE ruang=? and thn_ajaran=? ");
		$query->bindValue(1, $ruang_kel);
		$query->bindValue(2, $tahun);
		$query->execute();
		return $query->fetch();
	}
	public function d_s_k2($ruang_kel){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas WHERE ruang =?");
		$query->bindValue(1, $ruang_kel);
		$query->execute();
		return $query->fetch();
	}
	public function d_s_k3($ruang_kel, $thun, $id_s){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas LEFT JOIN data_siswa ON data_siswa.id_siswa=detail_siswa_kelas.id_siswa LEFT JOIN karyawan ON karyawan.id_karyawan=detail_siswa_kelas.id_wali WHERE ruang =? and thn_ajaran=? and detail_siswa_kelas.id_siswa=?");
		$query->bindValue(1, $ruang_kel);
		$query->bindValue(2, $thun);
		$query->bindValue(3, $id_s);
		$query->execute();
		return $query->fetch();
	}
	public function siswa(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM data_siswa ORDER BY id_siswa ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_siswa($id_s){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM data_siswa WHERE id_siswa = ?");
		$query->bindValue(1, $id_s);
		$query->execute();
		return $query->fetch();
	}
	public function tahun_ajaran(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM tahun_ajaran ORDER BY id_ta DESC");
		$query->execute();
		return $query->fetchAll();
	}
	public function tingkat(){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM tingkat ORDER BY id_tingkat ASC");
		$query->execute();
		return $query->fetchAll();
	}
	public function detail_kelas_s($id_s){
		global $pdo;
		$query = $pdo->prepare("SELECT * FROM detail_siswa_kelas LEFT JOIN karyawan ON karyawan.id_karyawan = detail_siswa_kelas.id_wali WHERE id_siswa = ? ORDER BY thn_ajaran DESC");
		$query->bindValue(1, $id_s);
		$query->execute();
		return $query->fetchAll();
	}
	
}
?>