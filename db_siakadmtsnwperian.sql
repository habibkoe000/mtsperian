-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2015 at 03:02 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_siakadmtsnwperian`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE IF NOT EXISTS `absensi` (
  `id_absensi` int(11) NOT NULL AUTO_INCREMENT,
  `id_siswa` varchar(200) NOT NULL,
  `id_kelas` varchar(20) NOT NULL,
  `absen` varchar(10) NOT NULL,
  `jam_pelajaran` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `thn_ajaran` varchar(50) NOT NULL,
  PRIMARY KEY (`id_absensi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id_absensi`, `id_siswa`, `id_kelas`, `absen`, `jam_pelajaran`, `tanggal`, `thn_ajaran`) VALUES
(6, 'S11', 'VII C', 'alfa', 2, '2014-08-21', ''),
(7, 'S11', 'VII C', 'hadir', 2, '2014-08-21', ''),
(8, 'S06', 'VII B', 'hadir', 3, '2014-08-21', ''),
(9, 'S07', 'VII B', 'alfa', 3, '2014-08-21', ''),
(10, 'S09', 'VII B', 'sakit', 3, '2014-08-21', ''),
(11, 'S01', 'VII B', 'sakit', 3, '2014-08-21', ''),
(12, 'S02', 'VII B', 'sakit', 3, '2014-08-21', ''),
(13, 'S06', 'VII B', 'sakit', 3, '2014-08-21', ''),
(14, 'S07', 'VII B', 'alfa', 3, '2014-08-21', ''),
(15, 'S09', 'VII B', 'bolos', 3, '2014-08-21', ''),
(16, 'S06', 'VII B', 'hadir', 3, '2014-08-21', ''),
(17, 'S07', 'VII B', 'sakit', 3, '2014-08-21', ''),
(18, 'S09', 'VII B', 'sakit', 3, '2014-08-21', ''),
(19, 'S01', 'VII B', 'hadir', 3, '2014-08-21', ''),
(20, 'S06', 'VII B', 'hadir', 3, '2014-08-21', ''),
(21, 'S09', 'VII B', 'hadir', 3, '2014-08-21', ''),
(22, 'S10', 'VII C', 'hadir', 1, '2014-08-21', '2009/2010'),
(23, 'S11', 'VII C', 'sakit', 1, '2014-08-21', '2009/2010'),
(24, 'S10', 'VII C', 'hadir', 1, '2014-08-22', '2009/2010'),
(25, 'S01', 'VII A', 'hadir', 1, '2014-08-22', '2009/2010'),
(26, 'S01', 'VII A', 'hadir', 1, '2014-08-22', '2009/2010'),
(27, 'S02', 'VII A', 'sakit', 1, '2014-08-22', '2009/2010'),
(28, 'S03', 'VII A', 'hadir', 1, '2014-08-22', '2009/2010'),
(29, 'S04', 'VII A', 'hadir', 1, '2014-08-22', '2009/2010'),
(30, 'S05', 'VII A', 'hadir', 1, '2014-08-22', '2009/2010'),
(31, 'S10', 'VII C', 'sakit', 1, '2014-08-22', '2009/2010'),
(32, 'S06', 'VII B', 'hadir', 2, '2014-08-23', '2009/2010'),
(33, 'S07', 'VII B', 'hadir', 2, '2014-08-23', '2009/2010'),
(34, 'S09', 'VII B', 'sakit', 2, '2014-08-23', '2009/2010'),
(35, 'S01', 'VII B', 'sakit', 2, '2014-08-23', '2009/2010'),
(36, 'S02', 'VII B', 'alfa', 2, '2014-08-23', '2009/2010'),
(37, 'S06', 'VII B', 'bolos', 2, '2014-08-23', '2009/2010'),
(38, 'S07', 'VII B', 'sakit', 2, '2014-08-23', '2009/2010'),
(39, 'S09', 'VII B', 'hadir', 2, '2014-08-23', '2009/2010'),
(40, 'S01', 'VII A', 'hadir', 1, '2014-08-24', '2009/2010'),
(41, 'S02', 'VII A', 'hadir', 1, '2014-08-24', '2009/2010'),
(42, 'S03', 'VII A', 'hadir', 1, '2014-08-24', '2009/2010'),
(43, 'S04', 'VII A', 'sakit', 1, '2014-08-24', '2009/2010'),
(44, 'S05', 'VII A', 'hadir', 1, '2014-08-24', '2009/2010'),
(45, 'S02', 'VII A', 'hadir', 2, '2014-09-13', '2009/2010'),
(46, 'S03', 'VII A', 'sakit', 2, '2014-09-13', '2009/2010'),
(47, 'S04', 'VII A', 'sakit', 2, '2014-09-13', '2009/2010'),
(48, 'S05', 'VII A', 'sakit', 2, '2014-09-13', '2009/2010'),
(49, 'S09', 'VII D', 'hadir', 3, '2014-09-13', '2014/2015'),
(50, 'S10', 'VII D', 'hadir', 3, '2014-09-13', '2014/2015'),
(51, 'S11', 'VII D', 'sakit', 3, '2014-09-13', '2014/2015'),
(52, 'S12', 'VII D', 'sakit', 3, '2014-09-13', '2014/2015'),
(53, 'S13', 'VII D', 'alfa', 3, '2014-09-13', '2014/2015'),
(54, 'S14', 'VII D', 'hadir', 3, '2014-09-13', '2014/2015');

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE IF NOT EXISTS `agama` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agama` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id`, `agama`) VALUES
(1, 'Islam'),
(2, 'Kristen'),
(3, 'Hindu'),
(4, 'Budha');

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `isi_berita` longtext NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_penulis` varchar(10) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `isi_berita`, `tanggal`, `id_penulis`, `foto`, `kategori`, `link`) VALUES
(1, 'Tes Error', '<h1 style="color: #ff0000;"><strong>saya yakin ini pasti error</strong></h1>', '2014-04-30 18:47:18', '1', '1398883638-bloam3.jpg', 'berita_umum', ''),
(18, 'Tes nulis admin Waduk Gunung Paok', '<p>waduk gunung paok</p>', '2014-05-04 06:16:21', '1', '1399184181-waduk2.jpg', 'berita_slide', ''),
(3, 'test dengan gambar', '<p>tes lagi</p>', '2014-04-30 19:12:56', '1', '1398885176-ultah tkj.jpg', 'berita_umum', ''),
(5, 'tulis baru lagi', '<p>tes lagi</p>', '2014-04-30 19:21:41', '1', '1398885701-Allah-Wallpaper.jpg', 'berita_umum', ''),
(7, 'tes reload lagi', '<p>tes reload</p>', '2014-05-02 07:41:02', '1', '1399016462-blurry-light-circles-abstract-hd-wallpaper-2560x1600-8080.jpg', 'berita_slide', ''),
(8, 'bisa reload gal tes editing', '<p>bisa gak ya bisa edit gak ya</p>', '2014-05-02 07:29:29', '1', '1399015769-100_1064.JPG', 'berita_umum', ''),
(9, 'kalo tes lagi edit maneh', '<p>bisa gak edit edit</p>', '2014-05-02 07:36:11', '1', '1399016171-1476342_657376444312484_374128537_n.jpg', 'berita_umum', ''),
(10, 'tes belajar edit lagi sekali', '<p>belajar edit</p>', '2014-05-02 07:34:24', '1', '1399016064-bloam4.jpg', 'berita_umum', ''),
(12, 'Mts Nw Perian Akreditasi 2', '<p>tes berita slide</p>', '2014-05-01 19:08:45', '1', '1398971282-slider.jpg', 'berita_slide', ''),
(13, 'Berita slide 2', '<p>tes yang ke tiga</p>', '2014-05-01 19:10:11', '1', '1398971411-slider2.jpg', 'berita_slide', ''),
(14, 'MTQ TINGAT NASIONAL TINGKAT SEKOLAH MENENGAH PERTAMA', '<p>anda bisa mengunjungi&nbsp;<a href="http://www.mtsnwperian.esy.es" target="_blank">http://www.mtsnwperian.esy.es</a></p>', '2014-05-01 20:44:49', '1', '1398977089-bgslider2.jpg', 'berita_slide', ''),
(28, 'tangal 4 februari 2015', '<p>tes berita</p>', '2015-02-04 07:48:15', '1', '1423036095-bukit pergasingan.jpg', 'berita_umum', ''),
(16, 'tes slider lagi ya dong', '<p>bisa gak ya</p>', '2014-07-31 07:11:42', '1', '1398977474-bloam3.jpg', 'berita_umum', ''),
(20, 'tes lagi dari guru Asror,,, tampilan metro windows 8', '<p>ini merupakan tampilan metro dari windows 8</p>', '2014-05-12 09:54:05', 'G01', '1399888445-startscreen.jpg', 'berita_umum', ''),
(19, 'tes nulis guru asror dengan judul yang panjang dan sangat panjang mau tau sampe mana mampunya', '<p>tes nulis sedikit</p>', '2014-05-04 06:36:24', 'G01', '1399185384-1483265_371850912960754_412409485_n.jpg', 'berita_umum', ''),
(21, 'Tes Pak taufik', '<p>tes buat dari pak taufik</p>', '2014-05-15 14:10:18', 'G02', '1400163018-calonwebsite.jpg', 'berita_umum', ''),
(22, 'Menyambut Nuzulul Qur''an 17 Ramadhan 1435 H', '<p>Ramadhan Tahun ini (1435 H)/ 2014 akan dilangsungkannya peringatan nuzulul Qur''an yang bertepatan pada tanggal 17 Ramadhan atau hari selasa 15 juli 2014</p>', '2014-09-01 03:33:21', '1', '1400168797-black-wallpaper-11.jpg', 'berita_umum', ''),
(23, 'Bandara Internasional Lombok', '<p>Bandara Internasional Lombok Merupakan Bandara baru yang terletak di daerah Lombok Tengah</p>', '2014-09-01 03:27:07', '1', '1400254053-juandaabud.jpg', 'berita_umum', ''),
(29, 'tes nulis berita dengan judul yang panjang sekali,, apabila tidak panjang ya harus panjang ', '<p>Membaca API Tiket.com</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>&nbsp;Diposting :&nbsp;Kamis, 28 Maret 2013</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>&nbsp;Kategori :&nbsp;PHP</p><br />\r\n<p>Malam ini bingung mau nulis apa, yasudah bikin script kaga jelas aja deh..:)</p><br />\r\n<p>Oke kali ini saya akan mencoba membaca data dari sebuah situs informasi yg lumayan terkenal yaitu</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>www.tiket.com</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Dimana di situs itu memuat berbagai macam informasi yg saya kira lumayan perlu untuk dikembangkan menjadi sebuah aplikasi yg powerfull jika kita bisa mengolah data yg ada pada situs itu.</p><br />\r\n<p>Berikut adalah kutipan langsung dari dokumentasi situs tersebut :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n<p>2</p><br />\r\n<p>3</p><br />\r\n<p>4</p><br />\r\n<p>5</p><br />\r\n<p>6</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>API development : api.master18.tiket.com</p><br />\r\n<p>API production : api.tiket.com</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>Tiket.com offers an all you can book experience. You can book flights, hotels, train, concerts and movies all inone place. And the good part is, they are all available through API!</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>API are available is JSON, XML and PHP serialized format.</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>There are three flow of payments that you can choose :</p><br />\r\n<ul><br />\r\n<li><strong>deposit</strong>: by putting money deposit at Tiket.com, you can manage your own transaction and payment. API will return insufficient fund if you have ran out of deposit. You can&rsquo;t continue using the API if you haven&rsquo;t deposit any.</li><br />\r\n<li><strong>tiket.com payment channels</strong>: Tiket.com offers diverse payment channel. You can ask user to pay using API (for bank transfer and KlikBCA), or redirect them to our payment gateway (for credit card and BCA KlikPay). You don&rsquo;t have to put any deposit if you are using this flow of payment.</li><br />\r\n<li><strong>direct contract with tiket.com</strong>: If you are a medium to large scale company, we can offer you contracting where we will invoice you bi-weekly.</li><br />\r\n</ul><br />\r\n<p>PS : API server development, has ssl error when use HTTPS,<br /> so for windows phone developer, we allow to use HTTP for development purpose.</p><br />\r\n<p>Generate Secret Key and Token</p><br />\r\n<p>Oke, pertama buatlah sebuah akun dengan meng-click&nbsp;<a href="http://www.tiket.com/affiliate">http://www.tiket.com/affiliate</a></p><br />\r\n<p>Kemudian masukan semua data-data anda lengkap sehingga ada akan bisa melakukan login pada</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p><a href="https://www.tiket.com/partner/login">https://www.tiket.com/partner/login</a></p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Setelah melakukan login, pada bagian API anda akan melihat secret key milik anda yg akan dipakai untuk mendapatkan token yg berfungsi sebagai hak akses anda membaca data dari API yg disediakan.</p><br />\r\n<p>Secret Key bisa anda lihat pada bagian :&nbsp;<strong>API secret key development</strong></p><br />\r\n<p>Setelah mendapatkan secret key, sekarang proses selanjutnya adalah generate Token. Pada address bar anda, silahkan copas link berikut :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>https://api.master18.tiket.com/apiv1/payexpress?method=getToken&amp;secretkey=DiisiDenganSeretKeyAnda</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Pada bagian akhir link diatas, terlihat saya memberi perintah untuk memasukan secret key milik anda. Contoh :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p><a href="https://api.master18.tiket.com/apiv1/payexpress?method=getToken&amp;secretkey=e10adc3949ba59abbe56e057f20f883e">https://api.master18.tiket.com/apiv1/payexpress?method=getToken&amp;secretkey=e10adc3949ba59abbe56e057f20f883e</a></p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Setelah mengisi scret key yg valid, tekan enter / proses. Anda kemudian akan mendapatkan return data&nbsp;<strong>TOKEN</strong>&nbsp;anda. Balasan yg didapat kira-kira seperti ini :</p><br />\r\n<p>Bentuk XML :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n<p>2</p><br />\r\n<p>3</p><br />\r\n<p>4</p><br />\r\n<p>5</p><br />\r\n<p>6</p><br />\r\n<p>7</p><br />\r\n<p>8</p><br />\r\n<p>9</p><br />\r\n<p>10</p><br />\r\n<p>11</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>&lt;tiket&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;output_type&gt;xml&lt;/output_type&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;diagnostic&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;elapstime&gt;0.1385&lt;/elapstime&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;memoryusage&gt;4.49MB&lt;/memoryusage&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;status&gt;200&lt;/status&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;lang&gt;en&lt;/lang&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;currency&gt;IDR&lt;/currency&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/diagnostic&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;token&gt;38d1996c567c688e1b285f464a12ea83&lt;/token&gt;</p><br />\r\n<p>&lt;/tiket&gt;</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Bentuk JSON :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>{"output_type":"json","diagnostic":{"elapstime":"0.1201","memoryusage":"4.46MB","status":"200","lang":"en","currency":"IDR"},"token":"38d1996c567c688e1b285f464a12ea83"}</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Biasanya sih dalam bentuk array, tapi bisa anda lihat token-nya seperti pada contoh : "token":"38d1996c567c688e1b285f464a12ea83".</p><br />\r\n<p>Sampai disini langkah anda mendapatkan&nbsp;<strong>token</strong>&nbsp;sudah selesai.</p><br />\r\n<p>Menggunakan API</p><br />\r\n<p>Okeh, pada proses ini kita akan menggunakan API yg disediakan. Pertama pelajari dalam-dalam dokumentasi yg diberikan oleh tiket.com sendiri</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p><a href="http://docs.tiket.com/">http://docs.tiket.com/</a></p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Disitu terpampang dengan jelas cara penggunaannya. Dan saya akan mencoba menggunakan API itu untuk membuat sebuah proses secara dinamis dengan menggunakan bantuan&nbsp;<a href="http://jquery.com/"><strong>jQuery</strong></a>&nbsp;untuk proses pembacaannya. Dengan jQuery, semua pekerjaan akan menjadi lebih mudah.</p><br />\r\n<p><em>Note :<br /> Bagi yg belum mengenal jQuery silahkan pelajari di om google atau semua referensi lainnya. Karena saya tidak akan membahas tentang cara menggunakan jQuery dan apa maksudnya</em></p><br />\r\n<p>Buatlah sebuah file dengan nama&nbsp;<strong>index.html</strong>&nbsp;dan letakan dalam folder xampp anda.</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n<p>2</p><br />\r\n<p>3</p><br />\r\n<p>4</p><br />\r\n<p>5</p><br />\r\n<p>6</p><br />\r\n<p>7</p><br />\r\n<p>8</p><br />\r\n<p>9</p><br />\r\n<p>10</p><br />\r\n<p>11</p><br />\r\n<p>12</p><br />\r\n<p>13</p><br />\r\n<p>14</p><br />\r\n<p>15</p><br />\r\n<p>16</p><br />\r\n<p>17</p><br />\r\n<p>18</p><br />\r\n<p>19</p><br />\r\n<p>20</p><br />\r\n<p>21</p><br />\r\n<p>22</p><br />\r\n<p>23</p><br />\r\n<p>24</p><br />\r\n<p>25</p><br />\r\n<p>26</p><br />\r\n<p>27</p><br />\r\n<p>28</p><br />\r\n<p>29</p><br />\r\n<p>30</p><br />\r\n<p>31</p><br />\r\n<p>32</p><br />\r\n<p>33</p><br />\r\n<p>34</p><br />\r\n<p>35</p><br />\r\n<p>36</p><br />\r\n<p>37</p><br />\r\n<p>38</p><br />\r\n<p>39</p><br />\r\n<p>40</p><br />\r\n<p>41</p><br />\r\n<p>42</p><br />\r\n<p>43</p><br />\r\n<p>44</p><br />\r\n<p>45</p><br />\r\n<p>46</p><br />\r\n<p>47</p><br />\r\n<p>48</p><br />\r\n<p>49</p><br />\r\n<p>50</p><br />\r\n<p>51</p><br />\r\n<p>52</p><br />\r\n<p>53</p><br />\r\n<p>54</p><br />\r\n<p>55</p><br />\r\n<p>56</p><br />\r\n<p>57</p><br />\r\n<p>58</p><br />\r\n<p>59</p><br />\r\n<p>60</p><br />\r\n<p>61</p><br />\r\n<p>62</p><br />\r\n<p>63</p><br />\r\n<p>64</p><br />\r\n<p>65</p><br />\r\n<p>66</p><br />\r\n<p>67</p><br />\r\n<p>68</p><br />\r\n<p>69</p><br />\r\n<p>70</p><br />\r\n<p>71</p><br />\r\n<p>72</p><br />\r\n<p>73</p><br />\r\n<p>74</p><br />\r\n<p>75</p><br />\r\n<p>76</p><br />\r\n<p>77</p><br />\r\n<p>78</p><br />\r\n<p>79</p><br />\r\n<p>80</p><br />\r\n<p>81</p><br />\r\n<p>82</p><br />\r\n<p>83</p><br />\r\n<p>84</p><br />\r\n<p>85</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>&lt;html&gt;</p><br />\r\n<p>&lt;head&gt;&lt;/head&gt;</p><br />\r\n<p>&lt;body&gt;</p><br />\r\n<p>&lt;scriptsrc="jquery-1.9.1.js"&gt;&lt;/script&gt;</p><br />\r\n<p>&lt;script&gt;</p><br />\r\n<p>$(document).ready(function() {</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;$(''#submit'').click(function(event) {</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(''#result'').html(''Proses Pengambilan Data . . .'');</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var FormData = $(''#FormTiket'').serialize();</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;event.preventDefault();</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$.ajax({</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;type : "GET",</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;url:''<a href="http://www.situs.com/tiket.php">http://www.situs.com/tiket.php</a>'',</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;data: FormData,</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cache: false,</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dataType: "jsonp",</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;success:function(data){</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if(data==''''){</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(''#result'').empty();</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(''#result'').html(''Maaf, data tidak ada untuk rute ini.'');</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}else{</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$(''#result'').empty();</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;var div = $("#result");</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;for(var i=0; i&lt;data[0].departures.result.length;i++){&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;div.append("Flight ID : "+data[0].departures.result[i].flight_id+"&lt;br&gt;");div.append("Nama Airlines : "+data[0].departures.result[i].airlines_name+"&lt;br&gt;");div.append("No. Penerbangan : "+data[0].departures.result[i].flight_number+"&lt;br&gt;");div.append("Harga Tiket : "+data[0].departures.result[i].price_value+"&lt;br&gt;");div.append("Jam Berangkat : "+data[0].departures.result[i].simple_departure_time+"&lt;br&gt;");div.append("Jam Tiba : "+data[0].departures.result[i].simple_arrival_time+"&lt;br&gt;");div.append("Durasi Perjalanan : "+data[0].departures.result[i].duration+"&lt;br&gt;");div.append("Gambar : "+data[0].departures.result[i].image+"&lt;br&gt;");div.append("&lt;hr&gt;");&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}&nbsp;&nbsp;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;})</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;});</p><br />\r\n<p>});</p><br />\r\n<p>&lt;/script&gt;</p><br />\r\n<p>&lt;formid="FormTiket"name="FormTiket"&gt;</p><br />\r\n<p>&lt;table&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Kota Asal&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;:&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;selectid="asal"name="asal"&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="CGK"&gt;Jakarta&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="BTH"&gt;Batam&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="DPS"&gt;Denpasar&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/select&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Kota Tujuan&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;:&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;selectid="tujuan"name="tujuan"&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="DPS"&gt;Denpasar&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="AMQ"&gt;Ambon&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="BPN"&gt;Balikpapan&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/select&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Tanggal&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;:&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;selectid="tanggal"name="tanggal"&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-01"&gt;01-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-02"&gt;02-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-03"&gt;03-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-04"&gt;04-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-05"&gt;05-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;optionvalue="2013-04-06"&gt;06-04-2013&lt;/option&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/select&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;tr&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;Proses&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;:&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;inputtype="submit"id="submit"name="submit"value="Cek"&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/td&gt;</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tr&gt;</p><br />\r\n<p>&lt;/table&gt;</p><br />\r\n<p>&lt;/form&gt;</p><br />\r\n<p>&lt;hr&gt;</p><br />\r\n<p>&lt;divid="result"&gt;</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>&lt;/div&gt;</p><br />\r\n<p>&lt;/body&gt;</p><br />\r\n<p>&lt;/html&gt;</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Seperti terlihat pada script diatas, saya meng-include-kan engine utama jQuery pada bagian</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>&lt;script src="jquery-1.9.1.js"&gt;&lt;/script&gt;</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Diharapkan anda bisa melihat dan menginclude-kan sendiri file jquery tersebut di folder xampp anda. Oke, setelah itu buat lagi sebuah file pada server anda dengan nama&nbsp;<strong>tiket.php</strong>&nbsp;dan isikan :</p><br />\r\n<p><a href="http://www.berthojoris.com/menu/blog/artikel/Membaca-API-Tiketcom.aspx">?</a></p><br />\r\n<table><br />\r\n<tbody><br />\r\n<tr><br />\r\n<td><br />\r\n<p>1</p><br />\r\n<p>2</p><br />\r\n<p>3</p><br />\r\n<p>4</p><br />\r\n<p>5</p><br />\r\n<p>6</p><br />\r\n<p>7</p><br />\r\n<p>8</p><br />\r\n<p>9</p><br />\r\n<p>10</p><br />\r\n<p>11</p><br />\r\n<p>12</p><br />\r\n<p>13</p><br />\r\n<p>14</p><br />\r\n<p>15</p><br />\r\n<p>16</p><br />\r\n<p>17</p><br />\r\n<p>18</p><br />\r\n<p>19</p><br />\r\n</td><br />\r\n<td><br />\r\n<p>&lt;?php</p><br />\r\n<p>$Asal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = $_GET[''asal''];</p><br />\r\n<p>$Tujuan&nbsp;&nbsp;&nbsp;&nbsp; = $_GET[''tujuan''];</p><br />\r\n<p>$Tanggal&nbsp;&nbsp;&nbsp; = $_GET[''tanggal''];</p><br />\r\n<p>$Token&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = ""; //Isi Token Anda Disini</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>$Data= file_get_contents("<a href="https://api.master18.tiket.com/search/flight?d=">https://api.master18.tiket.com/search/flight?d=</a>".$Asal."&amp;a=".$Tujuan."&amp;date=".$Tanggal."&amp;adult=1&amp;child=0&amp;infant=0&amp;ret_date=&amp;token=".$Token."&amp;output=json");</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>$Proses2= json_decode($Data);</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>$array= array();</p><br />\r\n<p>$array[] = (object)$Proses2;</p><br />\r\n<p>&nbsp;</p><br />\r\n<p>if($_GET[''callback'']) {</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo$_GET[''callback''] . ''(''.json_encode($array).'')'';</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;}else{</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo''{"items":''. json_encode($array) .''}'';</p><br />\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p><br />\r\n<p>?&gt;</p><br />\r\n</td><br />\r\n</tr><br />\r\n</tbody><br />\r\n</table><br />\r\n<p>Diatas anda dapat melihat perintah saya untuk mengisikan&nbsp;<strong>TOKEN&nbsp;</strong>yg sebelumnya anda buat pada bagian awal tutorial ini. Dan arahkan&nbsp;<strong>URL</strong>&nbsp;pada jQuery anda agar sesuai dengan&nbsp;<strong>PATH</strong>&nbsp;server anda.</p><br />\r\n<p>Hmmm...setelah saya mencobanya dan hasilnya adalah :</p><br />\r\n<p>Langkah 1 [Tampilan Awal]</p><br />\r\n<p>Langkah 2 [Ketika Tombol Cek Di Klik]</p><br />\r\n<p>Langkah 3 [Hasilnya]</p><br />\r\n<p>Script diatas bisa anda manfaatkan untuk aplikasi yg canggih dan tentunya mendapatkan pemasukan..:)</p><br />\r\n<p>Soo...Saatnya untuk bilang . . . . . . #HappyCoding</p><br />\r\n<p>&nbsp;</p>', '2015-02-14 01:06:21', '1', '1423875981-lumba.jpg', 'berita_umum', ''),
(30, 'tes judul berita yang panjang lagi, mungkin bisa dia tanpa ada yang turun ke bawah, hehehe, mudahan gak ada masalah si', '<p>tes due telu epmat</p>', '2015-02-24 02:38:27', '1', '1424745507-Boneka-Kura-kura-L.jpg', 'berita_umum', '');

-- --------------------------------------------------------

--
-- Table structure for table `dakwah_siswa`
--

CREATE TABLE IF NOT EXISTS `dakwah_siswa` (
  `id_dakwah` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(20) NOT NULL,
  `isi` text NOT NULL,
  `id_penulis` varchar(50) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_dakwah`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `dakwah_siswa`
--

INSERT INTO `dakwah_siswa` (`id_dakwah`, `judul`, `isi`, `id_penulis`, `tanggal`) VALUES
(1, 'Hidup Sementara', '<p>Hidup di dunia hanya sementara saudaraku, jadi jangan kita menghambur-hamburkan waktu, hingga nanti kita akan menuju kematian, tempat kehidupan yang hakiki, dan tempat kita akan hidup selama-lamanya, ya benar, itulah Surga, tempat kekekalan hidup, sedangkan dunia ini hanya sementara, dan tentu kita pasti akan menemui yang namanya mati</p>', 'S02', '2014-05-05 11:38:06'),
(2, 'Sabar Dan Tawakkal', '<p>Dalam menghadapi hidup yang tidak menentu ini, kadang kita berada di atas, kadang juga di bawah, oleh karena itu kita harus dan perlu memiliki sikap Lapang dada, kita harus sabar dan ikhlas menghadapinya di kala Allah memberikan cobaan berupa kesedihan, dan tetep intinya kita tidak boleh menyerah dan putus asa, kita harus selalu berusaha dan berpositif thinking terhadap apa yang Allah berikan pada kita baik itu berupa kebaikan maupun keburukan,, karena Allah tahu pasti, mana yang terbaik bagi kita, entah itu buruk, ya kita jadikan sebagai pelajaran untuk melatih kesabaran kita, kal pun kebahagiaan tentu kita tidak boleh sombong dan terlalu jumawa akan hal itu, karena itu merupakan ujian juga, apakah kita tetap ingat kepada Allah setelah di beri kebahagiaan atau kah kita lalai dan lupa bahwa semua yang telah di berikan itu datang nya dari Allah bukan dari hasil diri kita, sekali lagi semuanya itu datang dari Allah</p>', 'S01', '2014-08-28 05:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `data_siswa`
--

CREATE TABLE IF NOT EXISTS `data_siswa` (
  `id_siswa` varchar(100) NOT NULL,
  `tahun_angkatan` varchar(50) NOT NULL,
  `nama_siswa` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tempat_lahir` varchar(230) NOT NULL,
  `alamat` text NOT NULL,
  `kelamin` varchar(10) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `sekolah_asal` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nama_wali` varchar(50) NOT NULL,
  `agama_wali` varchar(20) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `penghasilan` varchar(100) NOT NULL,
  `alamat_wali` text NOT NULL,
  `telpon_wali` varchar(20) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_siswa`
--

INSERT INTO `data_siswa` (`id_siswa`, `tahun_angkatan`, `nama_siswa`, `tgl_lahir`, `tempat_lahir`, `alamat`, `kelamin`, `agama`, `kewarganegaraan`, `sekolah_asal`, `foto`, `email`, `nama_wali`, `agama_wali`, `pendidikan`, `pekerjaan`, `penghasilan`, `alamat_wali`, `telpon_wali`) VALUES
('S01', '2009/2010', 'Muhammad Habiburrahman ', '1993-10-05', 'Orong lekok, Keluncing Desa Perian', 'Orong lekok Dusun Keluncing, Desa Perian Kecamatan Montong Gading Lombok Timur NTB Indonesia                                ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 04 PERIAN (KELUNCING)', '1400429259-upload3.jpg', 'habibsantri@gmail.com', 'Ir. Muh. Suaedy', 'Islam', 'Strata (S1)', 'Wiraswasta', '1.500.000 - 2.000.000', 'Orong lekok Dusun Keluncing, Desa Perian Kecamatan Montong Gading Lombok Timur NTB                                       ', '081703714575'),
('S02', '2009/2010', 'Ulfia Nisrin', '1994-04-28', 'Perian selatan, RT 04', 'Perian RT 04, Desa perian, kecamatan montong gading lombok timur NTB Indonesia      \r\n                                                                        ', 'Perempuan', 'Islam', 'Indonesia', 'MI NW PERIAN 01', '1400429463-ririn.jpg', 'ulfianisrin@yahoo.co.id', 'TGH. Nasrullah Ma''sum', 'Islam', 'Strata (S1)', 'PNS', '3.000.000 - 4.000.000', 'Perian RT 04, Desa perian, kecamatan montong gading lombok timur NTB                                                  ', '08994464868'),
('S03', '2009/2010', 'hudaini handayani', '1994-07-25', 'sembalun bumbung,', 'sembalun bumbung        \r\n                                ', 'Perempuan', 'Islam', 'Indonesia', 'SDN 01 SEMBALUN BUMBUNG', '1395596224-1400332_649281128445442_1767818184_o.jpg', 'hudaini@yahoo.com', 'Ikhwanul', 'Islam', 'Strata (S1)', 'Petani', '1.500.000 - 2.000.000', 'sembalun bumbung        \r\n                                ', '083637484912'),
('S04', '2009/2010', 'Lili Any Wirastuti', '1995-08-14', 'Orong lekok', 'Orong Lekok keluncing desa perian      \r\n        ', 'Perempuan', 'Islam', 'Indonesia', 'SDN 04 Perian', 'default.jpg', 'lili@yahoo.com', 'hasan mashuri', 'Islam', 'SMA', 'Wiraswasta', '1.500.000 - 2.000.000', 'Orong lekok       \r\n        ', '0837394739237'),
('S05', '2009/2010', 'Sulthan Alvian', '1994-04-22', 'Terawangan', 'Terawangan, Dusun Selakerat Desa perian, kecamatan montong gading lombok timur NTB      \r\n                                ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 04 Perian', '1395590731-Kang_Min_Hyuk_04.jpg', 'vian@yahoo.com', 'H Muh ihsan', 'Islam', 'Strata (S1)', 'Wiraswasta', '1.500.000 - 2.000.000', 'Terawangan, Dusun Selakerat Desa perian, kecamatan montong gading lombok timur NTB      \r\n                                ', '08272637362'),
('S06', '2009/2010', 'Mursidin', '1994-06-14', 'Banggle', 'Banggle        \r\n        ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 5 Montong Betok', 'default.jpg', 'mursidin@yahoo.com', 'Inak Mur', 'Islam', 'SMA', 'Petani', '1.500.000 - 2.000.000', 'banggle        \r\n        ', '03847947383'),
('S07', '2009/2010', 'M Alwani', '1993-12-31', 'serijata,', 'serijata        \r\n                                ', 'Laki-Laki', 'Islam', 'Indonesia', 'MI NW Serijata', '1395590680-SR8Ta.jpg', 'alwani@yahoo.com', 'inak Alwani', 'Islam', 'SMA', 'Wiraswasta', '1.500.000 - 2.000.000', 'serijata        \r\n                                ', '0873645373'),
('S09', '2009/2010', 'Najamuddin', '1993-07-08', 'lekong rempung', 'lekong rempung        \r\n        ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 8 Montong Betok', 'default.jpg', 'najam@yahoo.com', 'inak najam', 'Islam', 'SMA', 'Wiraswasta', '1000.000 - 1.500.000', 'lekong rempung        \r\n        ', '0827338292628'),
('S11', '2009/2010', 'Sopian Hadi', '1993-05-12', 'Terawangan', 'Terawangan      \r\n        ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 04 Perian', '1395560040-suzy.jpg', 'sopianha@yahoo.com', 'Samsudin', 'Islam', 'Strata (S1)', 'Guru', '1.500.000 - 2.000.000', 'Terawangan     \r\n        ', '08765765433'),
('S10', '2012/2013', 'rima', '1998-06-30', 'Orong Lekok', 'Orong Lekok, Keluncing Desa Perian      \r\n                ', 'Perempuan', 'Islam', 'Indonesia', 'SDN 04 Perian', '1395559674-G7YTY.jpg', 'rima@yahoo.com', 'Ir Muh. Suaedy', 'Islam', 'Strata (S1)', 'Petani', '2.000.000 - 2.500.000', 'Orong Lekok, Keluncing Desa perian kecamatan montong Gading Lombok timur      \r\n                ', '087654758693'),
('S12', '2010/2011', 'Nisa', '1995-06-16', 'perian', 'perian        \r\n                ', 'Perempuan', 'Islam', 'Indonesia', 'MI NW PERIAN', '1395592238-bae-suzy.jpg', 'nisa@yahoo.com', 'Nina kirana', 'Islam', 'SMA', 'Wiraswasta', '1.500.000 - 2.000.000', 'perian        \r\n                ', '07862837973'),
('S13', '2012/2013', 'Nona', '1996-05-06', 'Jenggik', 'jenggik        \r\n        ', 'Perempuan', 'Islam', 'Indonesia', 'SDN 5 Terara', '1395592448-dinda kirana.jpg', 'nona@yahoo.com', 'nenda', 'Islam', 'Strata (S1)', 'PNS', '2.500.000 - 3.000.000', 'jenggik        \r\n        ', '0839373837'),
('S14', '2013/2014', 'Muhaimin', '1997-07-08', 'Terara', 'Terara       \r\n        ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 1 Terara', '1395595506-kuroko 4.jpg', 'muh@yahoo.com', 'muhdin', 'Islam', 'Strata (S1)', 'Guru', '1000.000 - 1.500.000', 'Terara        ', '02928292'),
('S15', '2011/2012', 'Khairul Ihsani', '1996-10-29', 'terawangan', 'terawangan        \r\n                ', 'Laki-Laki', 'Islam', 'Indonesia', 'SDN 04 Perian', '1395643973-531760_346986748709943_1418975891_n.jpg', 'erul@yahoo.com', 'Husni', 'Islam', 'Strata (S1)', 'PNS', '2.000.000 - 2.500.000', 'terawangan        \r\n                ', '083739373'),
('S16', '2009/2010', 'Dewi Salma', '1995-04-05', 'Lekong Lima', 'Lekong Lima', 'Perempuan', 'Islam', 'Indonesia', 'SDN 01 Montong Betok', '1395742366-momoi.jpg', 'dewi@gmail.com', 'Suryadi S.Pd', 'Islam', 'Strata (S1)', 'Guru Negeri', '2.000.000 - 2.500.000', 'Lekong Lima', '0273639373'),
('S17', '2010/2011', 'Samsul Arifin', '1993-06-15', 'Talun Ambon', 'Talun ambon        \r\n        ', 'Laki-Laki', 'AGAMA', 'Indonesia', 'SDN 5 kilang', '1404196869-akashi zone.png', 'arifin@yahoo.com', 'husni', 'AGAMA', 'Strata (S1)', 'Guru', '2.000.000 - 2.500.000', 'talun ambon        \r\n        ', '082738373'),
('S18', '2009/2010', 'si', '1994-07-08', 'lom', 'lom', 'Perempuan', 'Islam', 'indo', 'sd', '1400256650-talent220_kayano.jpg', 'si@ya.co', 'sino', 'Islam', 'Strata (S1)', 'PNS', '1.000.000', 'lom', '0289484783');

-- --------------------------------------------------------

--
-- Table structure for table `detail_siswa_kelas`
--

CREATE TABLE IF NOT EXISTS `detail_siswa_kelas` (
  `id_detail` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL,
  `ruang` varchar(50) NOT NULL,
  `thn_ajaran` varchar(50) NOT NULL,
  `id_siswa` varchar(50) NOT NULL,
  `id_wali` varchar(50) NOT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `detail_siswa_kelas`
--

INSERT INTO `detail_siswa_kelas` (`id_detail`, `kelas`, `ruang`, `thn_ajaran`, `id_siswa`, `id_wali`) VALUES
(49, 'VII', 'VII A', '2009/2010', 'S02', 'G02'),
(50, 'VII', 'VII A', '2009/2010', 'S03', 'G02'),
(51, 'VII', 'VII A', '2009/2010', 'S04', 'G02'),
(52, 'VII', 'VII A', '2009/2010', 'S05', 'G02'),
(61, 'VII', 'VII C', '2009/2010', 'S10', 'G04'),
(62, 'VII', 'VII C', '2009/2010', 'S11', 'G07'),
(63, 'VII', 'VII D', '2009/2010', 'S13', 'G08'),
(64, 'VIII', 'VIII A', '2010/2011', 'S01', 'G04'),
(65, 'VIII', 'VIII A', '2010/2011', 'S02', 'G04'),
(66, 'VIII', 'VIII A', '2010/2011', 'S04', 'G04'),
(67, 'VIII', 'VIII A', '2010/2011', 'S05', 'G04'),
(68, 'VIII', 'VIII A', '2010/2011', 'S06', 'G04'),
(69, 'VIII', 'VIII A', '2010/2011', 'S07', 'G04'),
(70, 'VIII', 'VIII A', '2010/2011', 'S09', 'G04'),
(71, 'VIII', 'VIII A', '2010/2011', 'S11', 'G04'),
(72, 'VIII', 'VIII A', '2010/2011', 'S12', 'G04'),
(73, 'VIII', 'VIII A', '2010/2011', 'S13', 'G04'),
(74, 'VIII', 'VIII A', '2010/2011', 'S14', 'G04'),
(75, 'VIII', 'VIII A', '2010/2011', 'S15', 'G04'),
(76, 'IX', 'IX A', '2010/2011', 'S01', 'G08'),
(77, 'IX', 'IX A', '2010/2011', 'S02', 'G08'),
(78, 'IX', 'IX A', '2010/2011', 'S03', 'G08'),
(79, 'IX', 'IX A', '2010/2011', 'S04', 'G08'),
(80, 'IX', 'IX A', '2010/2011', 'S06', 'G08'),
(81, 'IX', 'IX A', '2010/2011', 'S09', 'G08'),
(82, 'IX', 'IX A', '2010/2011', 'S10', 'G08'),
(83, 'IX', 'IX A', '2010/2011', 'S11', 'G08'),
(84, 'IX', 'IX A', '2010/2011', 'S13', 'G08'),
(85, 'IX', 'IX A', '2010/2011', 'S14', 'G08'),
(86, 'IX', 'IX A', '2010/2011', 'S15', 'G08'),
(87, 'IX', 'IX A', '2010/2011', 'S16', 'G08'),
(88, 'VIII', 'VIII A', '2014/2015', 'S04', 'G08'),
(89, 'VIII', 'VIII A', '2014/2015', 'S05', 'G08'),
(90, 'VIII', 'VIII A', '2014/2015', 'S06', 'G08'),
(91, 'VIII', 'VIII A', '2014/2015', 'S07', 'G08'),
(92, 'VIII', 'VIII A', '2014/2015', 'S09', 'G08'),
(93, 'VIII', 'VIII A', '2014/2015', 'S10', 'G08'),
(94, 'VII', 'VII A', '2014/2015', 'S01', 'G06'),
(96, 'VII', 'VII A', '2014/2015', 'S03', 'G06'),
(97, 'VII', 'VII A', '2014/2015', 'S04', 'G06'),
(98, 'VII', 'VII A', '2014/2015', 'S05', 'G06'),
(100, 'VII', 'VII D', '2014/2015', 'S09', 'G06'),
(101, 'VII', 'VII D', '2014/2015', 'S10', 'G06'),
(102, 'VII', 'VII D', '2014/2015', 'S11', 'G06'),
(103, 'VII', 'VII D', '2014/2015', 'S12', 'G06'),
(104, 'VII', 'VII D', '2014/2015', 'S13', 'G06'),
(105, 'VII', 'VII D', '2014/2015', 'S14', 'G06'),
(106, 'VII', 'VII B', '2009/2010', 'S12', 'G02'),
(107, 'VII', 'VII B', '2009/2010', 'S14', 'G02'),
(108, 'VII', 'VII B', '2009/2010', 'S17', 'G02'),
(109, 'VII', 'VII B', '2009/2010', 'S18', 'G02');

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE IF NOT EXISTS `fasilitas` (
  `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_fasilitas` varchar(50) NOT NULL,
  `pembahasan` text NOT NULL,
  `foto` varchar(200) NOT NULL,
  PRIMARY KEY (`id_fasilitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`id_fasilitas`, `nama_fasilitas`, `pembahasan`, `foto`) VALUES
(2, 'Ruang Belajar', '<p>Kelas</p>', '1396805728-556823_562523503764058_599029986_n.jpg'),
(4, 'Lapangan Sepak Bola', '<p>Fasilitas Olah raga</p>', '1396805798-556823_562523510430724_1249368188_n.jpg'),
(5, 'Perpustakaan', '<p>perpustakaan</p>', '1396805819-522260_562522780430797_1453899495_n.jpg'),
(6, 'Lapangan Voly', '<p>voly</p>', '1396805886-522260_562522783764130_898300401_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_file` varchar(150) NOT NULL,
  `nama_file` varchar(230) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_penulis` varchar(50) NOT NULL,
  `kategori` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `judul_file`, `nama_file`, `tanggal`, `id_penulis`, `kategori`) VALUES
(1, 'Download Silabus Bahasa Indonesia', '1399895090-1395636591_INFORMASI_AWAL_SBMPTN_2.pdf', '2014-05-12 11:56:27', 'G01', 'silabus'),
(2, 'Download Materi Biologi', '1399896033-Arti Penting Gulma.ppt', '2014-05-12 12:00:33', 'G01', 'download');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id_g` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi_g` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `gambar` varchar(200) NOT NULL,
  PRIMARY KEY (`id_g`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id_g`, `deskripsi_g`, `tanggal`, `gambar`) VALUES
(3, 'TES GALERY 1', '2014-05-12 06:45:51', '1399877151-1389246.jpg'),
(4, 'TES GALERY 2', '2014-05-12 06:48:04', '1399877284-124_1_4-Photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE IF NOT EXISTS `hari` (
  `id_h` int(11) NOT NULL AUTO_INCREMENT,
  `hari` varchar(100) NOT NULL,
  PRIMARY KEY (`id_h`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id_h`, `hari`) VALUES
(1, 'senin'),
(2, 'selasa'),
(3, 'rabu'),
(4, 'kamis'),
(5, 'jum''at'),
(6, 'sabtu'),
(7, 'ahad');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_belajar`
--

CREATE TABLE IF NOT EXISTS `jadwal_belajar` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL,
  `ruangan` varchar(50) NOT NULL,
  `id_smester` int(11) NOT NULL,
  `id_mp` varchar(20) NOT NULL,
  `hari` varchar(50) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `tahun` varchar(20) NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `jadwal_belajar`
--

INSERT INTO `jadwal_belajar` (`id_jadwal`, `kelas`, `ruangan`, `id_smester`, `id_mp`, `hari`, `jam_mulai`, `jam_selesai`, `tahun`) VALUES
(9, 'VII', 'VII A', 1, 'BI', 'senin', '07:30:00', '09:00:00', '2009/2010'),
(10, 'VII', 'VII A', 1, 'MAT', 'senin', '09:00:00', '09:45:00', '2009/2010'),
(11, 'VII', 'VII A', 1, 'BING', 'senin', '10:15:00', '11:45:00', '2009/2010'),
(12, 'VII', 'VII A', 1, 'FSK', 'selasa', '07:30:00', '09:00:00', '2009/2010'),
(13, 'VII', 'VII A', 1, 'FIQ', 'selasa', '09:00:00', '09:45:00', '2009/2010'),
(14, 'VII', 'VII A', 1, 'QRDS', 'selasa', '10:15:00', '11:45:00', '2009/2010'),
(15, 'VII', 'VII A', 1, 'AQDH', 'selasa', '11:45:00', '12:45:00', '2009/2010'),
(16, 'VII', 'VII A', 1, 'PNJ', 'rabu', '07:30:00', '09:00:00', '2009/2010'),
(17, 'VII', 'VII A', 1, 'FSK', 'rabu', '09:00:00', '11:00:00', '2009/2010'),
(18, 'VII', 'VII A', 1, 'PKN', 'rabu', '11:00:00', '12:30:00', '2009/2010'),
(19, 'VII', 'VII A', 1, 'FIQ', 'kamis', '07:30:00', '09:00:00', '2009/2010'),
(20, 'VII', 'VII A', 1, 'QRDS', 'kamis', '09:00:00', '11:00:00', '2009/2010'),
(21, 'VII', 'VII A', 1, 'AQDH', 'kamis', '11:00:00', '12:30:00', '2009/2010'),
(22, 'VII', 'VII A', 1, 'BLG', 'sabtu', '07:30:00', '09:00:00', '2009/2010'),
(23, 'VII', 'VII A', 1, 'BING', 'sabtu', '09:00:00', '11:00:00', '2009/2010'),
(24, 'VII', 'VII A', 1, 'BI', 'sabtu', '11:00:00', '12:30:00', '2009/2010'),
(25, 'VII', 'VII A', 1, 'AQDH', 'ahad', '07:30:00', '09:00:00', '2009/2010'),
(26, 'VII', 'VII A', 1, 'QRDS', 'ahad', '09:00:00', '11:00:00', '2009/2010'),
(27, 'VII', 'VII A', 1, 'BI', 'ahad', '11:00:00', '12:30:00', '2009/2010'),
(28, 'VII', 'VII A', 1, 'BLG', 'senin', '11:45:00', '12:30:00', '2009/2010'),
(29, 'VII', 'VII B', 1, 'BI', 'senin', '07:30:00', '08:15:00', '2009/2010'),
(30, 'VII', 'VII B', 1, 'MAT', 'senin', '08:15:00', '09:00:00', '2009/2010'),
(31, 'VII', 'VII B', 1, 'MAT', 'senin', '09:00:00', '09:45:00', '2009/2010'),
(32, 'VII', 'VII B', 1, 'FSK', 'senin', '10:15:00', '11:00:00', '2009/2010'),
(33, 'VII', 'VII B', 1, 'FSK', 'senin', '11:00:00', '11:45:00', '2009/2010'),
(34, 'VII', 'VII B', 1, 'AQDH', 'senin', '11:45:00', '12:30:00', '2009/2010'),
(35, 'VII', 'VII B', 1, 'PKN', 'selasa', '07:30:00', '08:15:00', '2009/2010'),
(36, 'VII', 'VII B', 1, 'PKN', 'selasa', '08:15:00', '09:00:00', '2009/2010'),
(37, 'VII', 'VII B', 1, 'PNJ', 'selasa', '09:00:00', '09:45:00', '2009/2010'),
(38, 'VII', 'VII B', 1, 'PNJ', 'selasa', '10:15:00', '11:00:00', '2009/2010'),
(39, 'VII', 'VII B', 1, 'BLG', 'selasa', '11:00:00', '11:45:00', '2009/2010'),
(40, 'VII', 'VII B', 1, 'ER1', 'selasa', '11:45:00', '12:30:00', '2009/2010'),
(41, 'VII', 'VII B', 1, 'BLG', 'rabu', '07:30:00', '08:15:00', '2009/2010'),
(42, 'VII', 'VII B', 1, 'BLG', 'rabu', '08:15:00', '09:00:00', '2009/2010'),
(43, 'VII', 'VII B', 1, 'FIQ', 'rabu', '09:00:00', '09:45:00', '2009/2010'),
(44, 'VII', 'VII B', 1, 'FIQ', 'rabu', '10:15:00', '11:00:00', '2009/2010'),
(45, 'VII', 'VII B', 1, 'QRDS', 'rabu', '11:00:00', '11:45:00', '2009/2010'),
(46, 'VII', 'VII B', 1, 'AQDH', 'rabu', '11:45:00', '12:30:00', '2009/2010'),
(47, 'VII', 'VII B', 1, 'QRDS', 'kamis', '07:30:00', '08:15:00', '2009/2010'),
(48, 'VII', 'VII B', 1, 'QRDS', 'kamis', '08:15:00', '09:00:00', '2009/2010'),
(49, 'VII', 'VII B', 1, 'AQDH', 'kamis', '09:00:00', '09:45:00', '2009/2010'),
(50, 'VII', 'VII B', 1, 'AQDH', 'kamis', '10:15:00', '11:00:00', '2009/2010'),
(51, 'VII', 'VII B', 1, 'ER1', 'kamis', '11:00:00', '11:45:00', '2009/2010'),
(52, 'VII', 'VII B', 1, 'ER1', 'kamis', '11:45:00', '12:30:00', '2009/2010'),
(53, 'VII', 'VII B', 1, 'MAT', 'sabtu', '07:30:00', '08:15:00', '2009/2010'),
(54, 'VII', 'VII B', 1, 'MAT', 'sabtu', '08:15:00', '09:00:00', '2009/2010'),
(55, 'VII', 'VII B', 1, 'BING', 'sabtu', '09:00:00', '09:45:00', '2009/2010'),
(56, 'VII', 'VII B', 1, 'BING', 'sabtu', '10:15:00', '11:00:00', '2009/2010'),
(57, 'VII', 'VII B', 1, 'FSK', 'sabtu', '11:00:00', '11:45:00', '2009/2010'),
(58, 'VII', 'VII B', 1, 'ER1', 'sabtu', '11:45:00', '12:30:00', '2009/2010'),
(59, 'VII', 'VII B', 1, 'AQDH', 'ahad', '07:30:00', '08:15:00', '2009/2010'),
(60, 'VII', 'VII B', 1, 'AQDH', 'ahad', '08:15:00', '09:00:00', '2009/2010'),
(61, 'VII', 'VII B', 1, 'QRDS', 'ahad', '09:00:00', '09:45:00', '2009/2010'),
(62, 'VII', 'VII B', 1, 'QRDS', 'ahad', '10:15:00', '11:00:00', '2009/2010'),
(63, 'VII', 'VII B', 1, 'ER1', 'ahad', '11:00:00', '11:45:00', '2009/2010'),
(64, 'VII', 'VII B', 1, 'ER1', 'ahad', '11:45:00', '12:30:00', '2009/2010');

-- --------------------------------------------------------

--
-- Table structure for table `jam_belajar`
--

CREATE TABLE IF NOT EXISTS `jam_belajar` (
  `id_jam` int(11) NOT NULL AUTO_INCREMENT,
  `awal` time NOT NULL,
  `ahir` time NOT NULL,
  PRIMARY KEY (`id_jam`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jam_belajar`
--

INSERT INTO `jam_belajar` (`id_jam`, `awal`, `ahir`) VALUES
(1, '07:30:00', '08:15:00'),
(2, '08:15:00', '09:00:00'),
(3, '09:00:00', '09:45:00'),
(5, '10:15:00', '11:00:00'),
(6, '11:00:00', '11:45:00'),
(8, '11:45:00', '12:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `jdwl`
--

CREATE TABLE IF NOT EXISTS `jdwl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(50) NOT NULL,
  `ruang` varchar(50) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `senin` varchar(50) NOT NULL,
  `selasa` varchar(50) NOT NULL,
  `rabu` varchar(50) NOT NULL,
  `kamis` varchar(50) NOT NULL,
  `jum'at` varchar(50) NOT NULL,
  `sabtu` varchar(50) NOT NULL,
  `ahad` varchar(50) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `tahun_ajaran` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE IF NOT EXISTS `karyawan` (
  `id_karyawan` varchar(20) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `tempat_lahir` varchar(230) NOT NULL,
  `kelamin` varchar(10) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `kewarganegaraan` varchar(100) NOT NULL,
  `status_pernikahan` varchar(50) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `instansi_pendidikan` varchar(50) NOT NULL,
  `telpon` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `status_jabatan` varchar(20) NOT NULL,
  `mulai_ngajar` date NOT NULL,
  `keahlian` text NOT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id_karyawan`, `foto`, `nama_lengkap`, `tgl_lahir`, `tempat_lahir`, `kelamin`, `agama`, `alamat`, `kewarganegaraan`, `status_pernikahan`, `pendidikan`, `instansi_pendidikan`, `telpon`, `email`, `status_jabatan`, `mulai_ngajar`, `keahlian`) VALUES
('G01', '1411589015-asror.jpg', 'Asror Ansori', '1991-04-16', 'Perian', 'Laki-Laki', 'Islam', 'Perian                                ', 'Indonesia', 'belum nikah', 'Strata (S1)', 'STKIP HAMZANWADI', '08373948393', 'asror@gmail.com', 'Guru', '2013-11-19', 'Fisika'),
('G02', '1395612783-484142_346913048717313_1510205162_n.jpg', 'Taufik HIdayat S.Ag', '0000-00-00', 'Orong Lekok', 'Laki-Laki', '', '                Orong lekok, RT 06 Keluncing                        ', '', '', '', '', '0817546378', 'Taufik@yahoo.com', 'Guru', '0000-00-00', ''),
('G03', '1410364617-bik uyun2.jpg', 'Nurul Hidayah', '1981-05-10', 'Selakerat', 'Perempuan', 'Islam', 'Selakerat                                ', 'Indonesia', 'sudah nikah', 'Strata (S1)', 'STKIP HAMZANWADI', '087263590812', 'hidayah@gmail.com', 'Guru', '2004-03-01', 'Bahasa Indonesia'),
('G04', '1395591752-in hae.png', 'Sandra', '0000-00-00', 'talun', 'Perempuan', '', '        talun                ', '', '', '', '', '0849494', 'san@gmail.com', 'Guru', '0000-00-00', ''),
('G05', '1410282551-pak malwi upload.jpg', 'Malwi Ama.Pd', '1963-10-10', 'Lekong Lima', 'Laki-Laki', 'Islam', 'Lekong Lima                        ', 'Indonesia', 'sudah nikah', 'Diploma 2(D2)', 'PGSD STKIP', '082987465378', 'Malwi@gmail.com', 'Guru', '1999-03-01', 'Olah Raga, Seni Budaya'),
('G06', '1395760004-kuroko2.jpg', 'Suryadi S.pjs', '0000-00-00', 'mataram, 23 januari 1987', 'Laki-Laki', '', '        mataram                ', '', '', '', '', '0839303839', 'sur@yahoo.com', 'Guru', '0000-00-00', ''),
('G07', '1395827212-jaejoong1.jpg', 'Samuel', '0000-00-00', 'Malang, 23, maret 1997', 'Laki-Laki', '', '        Malang                ', '', '', '', '', '0843774493', 'samuel@yahoo.com', 'Guru', '0000-00-00', ''),
('G08', '1411589211-ojanupload.jpg', 'Fauzan Azmi', '1990-06-12', 'Keluncing', 'Laki-Laki', 'Islam', 'Keluncing            ', 'Indonesia', 'belum nikah', 'SMA', 'MA NW PERIAN', '0873837373', 'Fauzan@gmail.com', 'Guru', '2011-06-14', 'Komputer'),
('G09', '1411588547-bik itok upload.jpg', 'Anita Harisanti', '1988-07-20', 'Terawangan', 'Perempuan', 'Islam', 'Terawangan', 'Indonesia', 'sudah nikah', 'Strata (S1)', 'STKIP HAMZANWADI', '089272927292', 'anitaharisanti@gmail.com', 'Guru', '2011-06-14', 'Matematika'),
('G10', '1410278025-pak sukardi upload.jpg', 'Sukardi SIP', '1976-03-12', 'Balik Batang', 'Laki-Laki', 'Islam', 'Balik Batang', 'Indonesia', 'sudah nikah', 'Strata (S1)', 'STKIP HAMZANWADI', '087283720280', 'Sukardi@gmail.com', 'Guru', '2004-03-01', 'Bahasa Arab'),
('P02', '1395596084-935672_10151465792902897_983153794_n.jpg', 'Hidayat Romdoni', '0000-00-00', 'Terawangan, 28 Mei 1989', 'Laki-Laki', '', '        Terawangan, dusun selakerat                ', '', '', '', '', '08373847388', 'hidayat@gmail.com', 'staff', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_berita`
--

CREATE TABLE IF NOT EXISTS `kategori_berita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kategori_berita`
--

INSERT INTO `kategori_berita` (`id`, `kategori`) VALUES
(1, 'berita_slide'),
(2, 'berita_umum'),
(3, 'berita_sekolah'),
(4, 'berita_hot');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE IF NOT EXISTS `kegiatan` (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `tggl_dari` date NOT NULL,
  `tggl_sampai` date NOT NULL,
  `tema_kegiatan` varchar(200) NOT NULL,
  `isi_kegiatan` text NOT NULL,
  `tanggal_posting` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `penulis` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kegiatan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `tggl_dari`, `tggl_sampai`, `tema_kegiatan`, `isi_kegiatan`, `tanggal_posting`, `penulis`) VALUES
(4, '2014-09-01', '2014-09-04', 'Festival Seni Tingkat Sekolah Menengah Pertama se Kecamatan Montong Gading', '<p>Kegiatan ini akan berlangsung di MTs NW Perian</p>', '2014-08-31 06:31:58', '1'),
(6, '2014-08-30', '2014-09-01', 'Hultah NWDI ke 79 di Pancor Lombok Timur', '<p>Kegiatan di ikuti oleh SMP/MTS/Sederajat , lomba akan di mulai pada pukul 07.00 wita - 11.00 wita</p>', '2014-08-31 06:19:27', '1'),
(7, '2014-09-01', '2014-09-03', 'Perayaan 17-an tingkat kecamatan Montong Gading', '<p>lokasi di kantor camat</p>', '2014-08-31 06:20:04', '1');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(100) NOT NULL,
  `tingkatan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama_ruang`, `tingkatan`) VALUES
(1, 'VII A', 'VII'),
(2, 'VII B', 'VII'),
(3, 'VII C', 'VII'),
(4, 'VII D', 'VII'),
(5, 'VIII A', 'VIII'),
(6, 'VIII B', 'VIII'),
(7, 'VIII C', 'VIII'),
(8, 'VIII D', 'VIII'),
(9, 'IX A', 'IX'),
(10, 'IX B', 'IX'),
(11, 'IX C', 'IX'),
(12, 'IX D', 'IX'),
(17, 'VII F', 'VII');

-- --------------------------------------------------------

--
-- Table structure for table `login_guru`
--

CREATE TABLE IF NOT EXISTS `login_guru` (
  `id_karyawan` varchar(50) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` varchar(50) NOT NULL,
  `id_karyawan_f` varchar(50) NOT NULL,
  PRIMARY KEY (`id_karyawan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_guru`
--

INSERT INTO `login_guru` (`id_karyawan`, `username`, `password`, `status`, `id_karyawan_f`) VALUES
('G01', 'asror', 'b84a4059d1af6c8b50bb7a28290dbd63', 'Guru', 'G01'),
('G02', 'taufik', 'd4305d7ed2ec97107cd6eb8dd4b6f6b7', 'Guru', 'G02'),
('G03', 'romanda', 'f2b09fa38a0fb674d87b1af482fac218', 'Guru', 'G03'),
('G05', 'malwi', '9cb83fc9e698e9d12a3064edb607d0c9', 'Guru', 'G05');

-- --------------------------------------------------------

--
-- Table structure for table `login_siswa`
--

CREATE TABLE IF NOT EXISTS `login_siswa` (
  `id_siswa` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL,
  `id_siswa_f` varchar(50) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_siswa`
--

INSERT INTO `login_siswa` (`id_siswa`, `username`, `password`, `status`, `id_siswa_f`) VALUES
('S01', 'habib', '1391921ec73a2f5dff54e075bbda7487', 'siswa', 'S01'),
('S02', 'ririn', 'b84a4059d1af6c8b50bb7a28290dbd63', 'santri', 'S02'),
('S15', 'erul', 'd1d1b8f0a0bef792d2f7737c897067bc', 'santri', 'S15'),
('S09', 'najam', '45884dba8c82f7d2607951b5820e6207', 'santri', 'S09');

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE IF NOT EXISTS `mata_pelajaran` (
  `id_mp` varchar(50) NOT NULL,
  `nama_mp` varchar(100) NOT NULL,
  `id_tingkat` varchar(50) NOT NULL,
  `id_karyawan` varchar(50) NOT NULL,
  PRIMARY KEY (`id_mp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id_mp`, `nama_mp`, `id_tingkat`, `id_karyawan`) VALUES
('BI', 'Bahasa Indonesia', 'VII', 'G01'),
('MAT', 'Matematika', 'VII', 'G07'),
('BING', 'Bahasa Inggris', 'VII', 'G01'),
('PKN', 'Pendidikan Kewarganegaraan', 'VII', 'G04'),
('FSK', 'Fisika', 'VII', 'G05'),
('BLG', 'Biologi', 'VII', 'G04'),
('PNJ', 'Pendidikan Jasmani Dan Kesehatan', 'VII', 'G06'),
('FIQ', 'FIQIH', 'VII', 'G08'),
('QRDS', 'Qur''an Hadist 1', 'VII', 'G07'),
('AQDH', 'Aqidah Akhlak', 'VII', 'G02'),
('BI2', 'Bahasa Indonesia jilid 2', 'VIII', 'G03'),
('MAT2', 'Matematika 2', 'VIII', 'G05'),
('BING2', 'Bahasa Inggris II', 'VIII', 'G01'),
('ER1', 'KOSONG', 'VII', ''),
('PKN2', 'Pendidikan Kewarganegaraan 2', 'VIII', 'G04'),
('ER2', 'KOSONG', 'VIII', 'PILIH GURU'),
('ER3', 'KOSONG', 'IX', 'PILIH GURU');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_pr`
--

CREATE TABLE IF NOT EXISTS `nilai_pr` (
  `id_siswa` varchar(20) NOT NULL,
  `thn_ajaran` varchar(23) NOT NULL,
  `id_smester` varchar(23) NOT NULL,
  `kelas` varchar(23) NOT NULL,
  `id_mp` varchar(23) NOT NULL,
  `nilai_PR` varchar(23) NOT NULL,
  `tggl_ujian` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_pr`
--

INSERT INTO `nilai_pr` (`id_siswa`, `thn_ajaran`, `id_smester`, `kelas`, `id_mp`, `nilai_PR`, `tggl_ujian`) VALUES
('S06', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S07', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S09', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S02', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S06', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S07', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S09', '2009/2010', '1', 'VII B', 'FSK', '70', '2010-02-09'),
('S10', '2009/2010', '1', 'VII C', 'BI', '50', '2014-09-13'),
('S11', '2009/2010', '1', 'VII C', 'BI', '50', '2014-09-13');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_smester`
--

CREATE TABLE IF NOT EXISTS `nilai_smester` (
  `id_siswa` varchar(20) NOT NULL,
  `thn_ajaran` varchar(20) NOT NULL,
  `id_smester` varchar(20) NOT NULL,
  `ruang_kelas` varchar(20) NOT NULL,
  `id_mp` varchar(20) NOT NULL,
  `nilai_smester` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_smester`
--

INSERT INTO `nilai_smester` (`id_siswa`, `thn_ajaran`, `id_smester`, `ruang_kelas`, `id_mp`, `nilai_smester`) VALUES
('S02', '2009/2010', '1', 'VII A', 'BI', '80'),
('S02', '2009/2010', '1', 'VII A', 'MAT', '80'),
('S02', '2009/2010', '1', 'VII A', 'BING', '80'),
('S02', '2009/2010', '1', 'VII A', 'PKN', '80'),
('S02', '2009/2010', '1', 'VII A', 'FSK', '80'),
('S02', '2009/2010', '1', 'VII A', 'BLG', '80'),
('S02', '2009/2010', '1', 'VII A', 'PNJ', '75'),
('S02', '2009/2010', '1', 'VII A', 'FIQ', '75'),
('S02', '2009/2010', '1', 'VII A', 'QRDS', '75'),
('S02', '2009/2010', '1', 'VII A', 'AQDH', '75'),
('S03', '2009/2010', '1', 'VII A', 'BI', '90'),
('S03', '2009/2010', '1', 'VII A', 'MAT', '90'),
('S03', '2009/2010', '1', 'VII A', 'BING', '90'),
('S03', '2009/2010', '1', 'VII A', 'PKN', '80'),
('S03', '2009/2010', '1', 'VII A', 'FSK', '80'),
('S03', '2009/2010', '1', 'VII A', 'BLG', '75'),
('S03', '2009/2010', '1', 'VII A', 'PNJ', '75'),
('S03', '2009/2010', '1', 'VII A', 'FIQ', '85'),
('S03', '2009/2010', '1', 'VII A', 'QRDS', '85'),
('S03', '2009/2010', '1', 'VII A', 'AQDH', '80'),
('S02', '2009/2010', '1', 'VII A', 'BI', '50'),
('S03', '2009/2010', '1', 'VII A', 'BI', '50'),
('S04', '2009/2010', '1', 'VII A', 'BI', '50'),
('S05', '2009/2010', '1', 'VII A', 'BI', '50'),
('S09', '2014/2015', '1', 'VII D', 'BI', '80'),
('S09', '2014/2015', '1', 'VII D', 'MAT', '80'),
('S09', '2014/2015', '1', 'VII D', 'BING', '80'),
('S09', '2014/2015', '1', 'VII D', 'PKN', '80'),
('S09', '2014/2015', '1', 'VII D', 'FSK', '70'),
('S09', '2014/2015', '1', 'VII D', 'BLG', '70'),
('S09', '2014/2015', '1', 'VII D', 'PNJ', '70'),
('S09', '2014/2015', '1', 'VII D', 'FIQ', '60'),
('S09', '2014/2015', '1', 'VII D', 'QRDS', '60'),
('S09', '2014/2015', '1', 'VII D', 'AQDH', '60');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_uh`
--

CREATE TABLE IF NOT EXISTS `nilai_uh` (
  `id_siswa` varchar(20) NOT NULL,
  `thn_ajaran` varchar(20) NOT NULL,
  `id_smester` varchar(20) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `id_mp` varchar(20) NOT NULL,
  `nilai_UH` varchar(20) NOT NULL,
  `tggl_ujian` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_uh`
--

INSERT INTO `nilai_uh` (`id_siswa`, `thn_ajaran`, `id_smester`, `kelas`, `id_mp`, `nilai_UH`, `tggl_ujian`) VALUES
('S02', '2009/2010', '1', 'VII A', 'BING', '80', '2010-01-28'),
('S03', '2009/2010', '1', 'VII A', 'BING', '80', '2010-01-28'),
('S04', '2009/2010', '1', 'VII A', 'BING', '80', '2010-01-28'),
('S05', '2009/2010', '1', 'VII A', 'BING', '80', '2010-01-28'),
('S10', '2009/2010', '1', 'VII C', 'BING', '70', '2014-09-13'),
('S11', '2009/2010', '1', 'VII C', 'BING', '70', '2014-09-13');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan_wali`
--

CREATE TABLE IF NOT EXISTS `pekerjaan_wali` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pekerjaan_wali`
--

INSERT INTO `pekerjaan_wali` (`id`, `pekerjaan`) VALUES
(1, 'PEGAWAI NEGERI SIPIL'),
(2, 'ABRI'),
(3, 'Guru Negeri'),
(4, 'Dosen'),
(5, 'Wiraswasta'),
(6, 'Pegawai Swasta'),
(7, 'Petani'),
(8, 'Buruh Tani'),
(9, 'Buruh Pabrik'),
(10, 'Guru Swasta');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_terahir`
--

CREATE TABLE IF NOT EXISTS `pendidikan_terahir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pendidikan_terahir`
--

INSERT INTO `pendidikan_terahir` (`id`, `pendidikan`) VALUES
(1, 'Tidak Sekolah'),
(2, 'Sekolah Dasar'),
(3, 'SMP'),
(4, 'SMA'),
(5, 'Strata (S1)'),
(6, 'Strata (S2)'),
(8, 'Strata(S3)'),
(9, 'Diploma 3(D3)'),
(10, 'Diploma 2(D2)');

-- --------------------------------------------------------

--
-- Table structure for table `penelitian_guru`
--

CREATE TABLE IF NOT EXISTS `penelitian_guru` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `isi` longtext NOT NULL,
  `file` varchar(200) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_penulis` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `penelitian_guru`
--

INSERT INTO `penelitian_guru` (`id`, `judul`, `isi`, `file`, `tanggal`, `id_penulis`) VALUES
(1, 'penelitian 1', '<p>tes penelitian</p>', 'No_Data.doc', '2014-05-05 13:07:31', 'G01'),
(2, 'penelitian ini dalam rangka membudidayakan tanaman kedelai', '<p>Penelitian 2</p>', '1399295438-53-196-1-PB.pdf', '2014-05-05 13:22:03', 'G01');

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan_wali`
--

CREATE TABLE IF NOT EXISTS `penghasilan_wali` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penghasilan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `penghasilan_wali`
--

INSERT INTO `penghasilan_wali` (`id`, `penghasilan`) VALUES
(1, '< 1000.000'),
(2, '1000.000 - 1.500.000'),
(3, '1.500.000 - 2.000.000'),
(4, '2.000.000 - 2.500.000'),
(5, '2.500.000 - 3.000.000'),
(6, '3.000.000 - 4.000.000'),
(7, '4.000.000 - 5.500.000');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE IF NOT EXISTS `pengumuman` (
  `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `isi_pengumuman` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `penulis` varchar(50) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pengumuman`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`id_pengumuman`, `judul`, `isi_pengumuman`, `file`, `kategori`, `penulis`, `tanggal`) VALUES
(8, 'Pengumuman Nulis', '<p>Pengumuman nulis ya</p>', '', 'tulisan', '1', '2014-08-26 13:30:26'),
(9, 'Pengumuman Lomba 17 Agustus ', '<p>Pengumuman,</p>\r\n<p>pemenang juara 1 : Muhammad Zainuddin,,</p>\r\n<p>Pemenang Juara 2 : Muhammad Syamsuri</p>', '', 'tulisan', '1', '2014-08-28 14:54:50'),
(11, 'Asror Tes Umumin', '<p>Tes ngumumin lagi</p>', '', 'tulisan', 'G01', '2014-09-08 09:32:50');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE IF NOT EXISTS `prestasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `penerima` varchar(200) NOT NULL,
  `tahun` date NOT NULL,
  `isi` text NOT NULL,
  `foto` varchar(200) NOT NULL,
  `id_penulis` varchar(50) NOT NULL,
  `status` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `judul`, `penerima`, `tahun`, `isi`, `foto`, `id_penulis`, `status`, `tanggal`) VALUES
(1, 'lomba anime', 'G03', '2014-09-09', '<p>tesr</p>', '1410370792-madara-vs-hashirama-animeipics.png', '2', 'guru', '2014-09-10 17:39:52'),
(2, 'Lomba membuat tembikar', 'G10', '2014-09-10', '<p>lomba buat tembikar</p>', '1410373629-tembikar3.jpg', '2', 'guru', '2014-09-10 18:27:09'),
(3, 'lomba makan kerupuk', 'G03', '2014-09-08', '<p>lomba makan kerupuk dalam rangka 17 agustus&nbsp;</p>', 'default.jpg', '2', 'guru', '2014-09-10 18:55:03');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_profil` varchar(150) NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `isi_profil` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `nama_profil`, `gambar`, `isi_profil`) VALUES
(1, 'Sambutan Kepala Sekolah', '1409480905-kamadupload.jpg', '<p>Sambutan kepala sekolah</p>'),
(2, 'Visi dan Misi', '1399269945-visi.jpg', '<p>visi dan misi</p>'),
(3, 'Sasaran Mutu', '1399269982-sasaran.jpg', '<p>Sasaran Mutu</p>'),
(4, 'Tujuan', '1399270020-tujuan.jpg', '<p>Tujuan</p>'),
(5, 'Moto', '1399270059-moto.jpg', '<p>Moto</p>'),
(6, 'Akreditasi', '1399270119-akreditasi.jpg', '<p>Akreditasi</p>'),
(8, 'kontak Info', '1399270155-kontak.jpg', '<p>kontak</p>');

-- --------------------------------------------------------

--
-- Table structure for table `smester`
--

CREATE TABLE IF NOT EXISTS `smester` (
  `id_smester` int(11) NOT NULL AUTO_INCREMENT,
  `nama_smester` varchar(20) NOT NULL,
  PRIMARY KEY (`id_smester`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `smester`
--

INSERT INTO `smester` (`id_smester`, `nama_smester`) VALUES
(1, 'Smester Ganjil'),
(2, 'Smester Genap');

-- --------------------------------------------------------

--
-- Table structure for table `standart_nilai`
--

CREATE TABLE IF NOT EXISTS `standart_nilai` (
  `nilai` varchar(20) NOT NULL,
  `huruf` varchar(50) NOT NULL,
  `predikat` varchar(50) NOT NULL,
  PRIMARY KEY (`nilai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standart_nilai`
--

INSERT INTO `standart_nilai` (`nilai`, `huruf`, `predikat`) VALUES
('0', 'Nol', 'Sangat Buruk'),
('20', 'dua puluh', 'buruk');

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE IF NOT EXISTS `tahun_ajaran` (
  `id_ta` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(200) NOT NULL,
  PRIMARY KEY (`id_ta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_ta`, `tahun`) VALUES
(1, '2009/2010'),
(2, '2010/2011'),
(3, '2011/2012'),
(4, '2012/2013'),
(5, '2013/2014'),
(6, '2014/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE IF NOT EXISTS `tingkat` (
  `id_tingkat` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tingkat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tingkat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`id_tingkat`, `nama_tingkat`) VALUES
(1, 'VII'),
(2, 'VIII'),
(3, 'IX');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `ussername` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `foto` varchar(200) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `ussername`, `password`, `foto`, `status`) VALUES
(1, 'Muhammad Habiburrahman', 'habibkoe', '27ed74d1e873f5842ff665584fad442e', '', 'administrator'),
(2, 'Ulfia Nisrin', 'ririn', 'b84a4059d1af6c8b50bb7a28290dbd63', '', 'administrator');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
