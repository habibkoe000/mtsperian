<div class="dala-menu-kiri">
        <div id="wrapper">

	<ul class="menu">
          <li class="item13"><a href="#">Data Umum</a>
        	<ul>
            	<li class="subitem1"><a href="?page=data_umum&aksi=agama&tambah">Agama</a></li>
                <li class="subitem2"><a href="?page=data_umum&aksi=pekerjaan&tambah">Pekerjaan Wali Murid</a></li>
            	<li class="subitem3"><a href="?page=data_umum&aksi=pendidikan&tambah">Pendidikan Terahir</a></li>
                <li class="subitem3"><a href="?page=data_umum&aksi=penghasilan&tambah">Penghasilah Wali Murid</a></li>
                <li class="subitem3"><a href="?page=data_umum&aksi=hari&tambah">Hari</a></li>
            </ul>
		</li>
		<li class="item5"><a href="#">Siswa</a>
			<ul>
				<li class="subitem1"><a href="?page=santri&aksi=tambah">Tambah Data Siswa</a></li>
				<li class="subitem2"><a href="?page=santri&aksi=lihat">Lihat Data Siswa</a></li>
			</ul>
		</li>
		<li class="item2"><a href="#">Kepegawaian</a>
			<ul>
				<li class="subitem1"><a href="?page=ustadz&aksi=tambah">Tambah Kepegawaian</a></li>
				<li class="subitem2"><a href="?page=ustadz&aksi=lihat">Lihat Data Pegawai</a></li>
			</ul>
		</li>
         <li class="item10"><a href="#">Kelas</a>
         <ul>
            	<li class="subitem1"><a href="?page=kelas&aksi=kelas1">Tambah Kelas</a></li>
                <li class="subitem2"><a href="?page=kelas&aksi=tambah_siswa">Tambah Siswa Ke Kelas</a></li>
            	<li class="subitem3"><a href="?page=kelas&aksi=lihat">Lihat Siswa Per Kelas</a></li>
                <li class="subitem5"><a href="?page=kelas&aksi=tahun_ajaran&tambah">Tahun Ajaran</a></li>
            </ul>
         </li>
         <li class="item11"><a href="#">Akademik</a>
        	<ul>
            	<li class="subitem1"><a href="?page=mapel&aksi=tambah&m_p">Mata Pelajaran</a></li>
                <li class="subitem2"><a href="?page=jadwal&aksi=tambah">Jadwal Per Kelas</a></li>
                <li class="subitem2"><a href="?page=lihat_jadwal&aksi=tambah">Lihat Jadwal Per Kelas</a></li>
            	<li class="subitem3"><a href="?page=nilai&aksi=tambah">Tambah Nilai Siswa</a></li>
				<li class="subitem4"><a href="?page=nilai_lihat&aksi=lihat_nilai">Lihat Nilai Siswa</a></li>
                <li class="subitem5"><a href="?page=absensi&aksi=lihat">Absensi Siswa</a></li>
                <li class="subitem5"><a href="?page=lihat_absensi&aksi=lihat">Lihat Absensi Siswa</a></li>
                <li class="subitem3"><a href="?page=data_umum&aksi=jam_belajar&ac=tmb">Jam Belajar</a></li>
            </ul>
		</li>
        <li class="item4"><a href="#">Berita</a>
			<ul>
				<li class="subitem1"><a href="?page=berita&aksi=tambah">Tambah Berita + </a></li>
				<li class="subitem2"><a href="?page=berita&aksi=lihat">Lihat Berita</a></li>
                <li class="subitem2"><a href="?page=berita&aksi=kategori">Kategori</a></li>
                <li class="subitem2"><a href="?page=berita&aksi=filter">Filter Tulisan </a></li>
			</ul>
		</li>
		<li class="item6"><a href="#">Pengumuman</a>
			<ul>
				<li class="subitem1"><a href="?page=pengumuman&aksi=opsi">Tambah Pengumuman</a></li>
				<li class="subitem2"><a href="?page=pengumuman&aksi=lihat">Lihat Pengumuman</a></li>
			</ul>
		</li>
		<li class="item8"><a href="#">Agenda Kegiatan</a>
        <ul>
				<li class="subitem1"><a href="?page=agenda&aksi=tambah">Tambah Agenda</a></li>
				<li class="subitem2"><a href="?page=agenda&aksi=lihat">Lihat Agenda</a></li>
			</ul>
		</li>
        <li class="item7"><a href="#">Prestasi & Penelitian</a>
        	<ul>
            	<li class="subitem1"><a href="?page=prestasi&aksi=prestasi_list">Tambah Prestasi Sekolah</a></li>
                <li class="subitem2"><a href="?page=prestasi&aksi=lihat">Lihat Prestasi Sekolah</a></li>
                 <li class="subitem2"><a href="?page=penelitian&aksi=p_lihat">Penelitian Sekolah</a></li>
            </ul>
		</li>
        <li class="item3"><a href="#">Fasilitas Sekolah</a>
        	<ul>
				<li class="subitem1"><a href="?page=fasilitas&aksi=tambah">Tambah Fasilitas</a></li>
				<li class="subitem2"><a href="?page=fasilitas&aksi=lihat">Lihat Fasilitas</a></li>
			</ul>
		</li>
        <li class="item12"><a href="#">Profil Sekolah</a>
        	<ul>
				<li class="subitem1"><a href="?page=profil&aksi=tambah">Tambah Profil</a></li>
				<li class="subitem2"><a href="?page=profil&aksi=lihat">Lihat Profil</a></li>
			</ul>
		</li>
         <li class="item1"><a href="#">Galery</a>
		</li>
         <li class="item14"><a href="#">Upload</a>
		</li>
	</ul>

</div>

<!--initiate accordion-->
<script type="text/javascript">
	$(function() {
	
	    var menu_ul = $('.menu > li > ul'),
	           menu_a  = $('.menu > li > a');
	    
	    menu_ul.hide();
	
	    menu_a.click(function(e) {
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	
	});
</script>
   
   	</div>
   <div class="footer-menu-kiri">
    Copyright &copy; MTs NW Perian 2014
    </div>
