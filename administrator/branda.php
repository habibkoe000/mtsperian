<?php

session_start();
include_once('../konek/coonnect.php');
include_once('../query/queryClass.php');

$dataClass = new queryClass;
$siswa_kelas = $dataClass->siswa_kelas();
$pegawai = $dataClass->pegawai();
$tahun_ajaran = $dataClass->tahun_ajaran();
$tingkat = $dataClass->tingkat();
$k_b = $dataClass->kat_b();
$agama = $dataClass->agama();
$pendidikan = $dataClass->pendidikan_terahir();
$pekerjaan_wali = $dataClass->pekerjaan_wali();
$siswa = $dataClass->siswa();
$penghasilan = $dataClass->penghasilan_wali();
$guru = $dataClass->guru();
$f_k = $dataClass->filter_kata();
$pengumuman = $dataClass->semua_pengumuman();
$fasilitas = $dataClass->fasilitas();
$profil = $dataClass->profil_sekolah();
$prestasi = $dataClass->prestasi();
$penelitian = $dataClass->penelitian();
$mapel = $dataClass->mapel();
$kegiatan = $dataClass->semua_kegiatan2();
$smester = $dataClass->smester();
$hari = $dataClass->hari();
$jam = $dataClass->jam();
if(isset($_SESSION['id'])){
	$id_user = $_SESSION['id'];
$pg = $_GET['page'];
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pg; ?> > admin mts nw perian </title>
<link rel="shortcut icon" href="../gambar/favicon.png" />
<link rel="stylesheet" href="asset/style.css" type="text/css" />
<link rel="stylesheet" href="asset/menu-samping.css" type="text/css" />
<script src="../js/jquery2.1.js" type="text/javascript"></script>
<script type="text/javascript" src="../tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "textarea#isi_body",
    theme: "modern",
    width: 995,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/Style.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 
</script>

</head>

<body>

<div class="header">
	<a href="?page=branda"><img src="gambar/logo.png" /></a>
    <div class="head_cari">
    <table class="tbl-cari">
    <tr><td>
    	<form>
        	<input type="text" name="cari" placeholder="cari..." class="form-cari" /><input class="tmb-cari" type="submit" name="caritmb" value="."  />
        </form>
        </td></tr>
        </table>
    </div>
    <div class="head-kanan">
    	<?php 
		global $pdo;
	$query = $pdo->prepare("SELECT nama, status, ussername FROM user WHERE id = ?");
	$query->bindValue(1, $id_user);
	$query->execute();	
	$baris = $query->fetch();
		echo '<a href="?page=admin_edit">';
		echo '<img src="gambar/setting.png" />';
		echo $baris['ussername']; 
		echo '</a>';
		?>
    </div>
    <div class="head_out">
    	<ul class="menu">
    	<li class="item9"><a href="logout.php">Logout</a>
        </li>
        </ul>
	</div>
</div>
	<div class="side-menu-kiri">
    	<?php include('template/sidebar-left.php'); ?>
    	    </div>
    <?php 
	
	if($pg == 'branda'){ 
		include('kontent/branda-awal.php');
	}
	else if($pg == 'berita'){
		include('kontent/berita.php');	
	}
	else if($pg == 'santri'){
		include('kontent/santri.php');	
	}
	else if($pg == 'ustadz'){
		include('kontent/ustadz.php');	
	}
	else if($pg == 'data_umum'){
		include('kontent/data_umum.php');	
	}else if($pg == 'kelas'){
		include('kontent/kelas.php');	
	}else if($pg == 'pengumuman'){
		include('kontent/pengumuman.php');	
	}else if($pg == 'admin_edit'){
		include('kontent/admin_edit.php');	
	}else if($pg == 'fasilitas'){
		include('kontent/fasilitas_sekolah.php');	
	}else if($pg == 'profil'){
		include('kontent/profil_sekolah.php');	
	}else if($pg == 'prestasi'){
		include('kontent/prestasi_sekolah.php');	
	} else if($pg == 'penelitian'){
		include('kontent/penelitian_guru.php');	
	}else if($pg == 'absensi'){
		include('kontent/absensi.php');	
	} else if($pg == 'agenda'){
		include('kontent/agenda_sekolah.php');	
	} else if($pg == 'error'){
		include('kontent/error.php');	
	}  else if($pg == 'mapel'){
		include('kontent/mata_pelajaran.php');	
	}  else if($pg == 'jadwal'){
		include('kontent/jadwal_belajar.php');	
	}   else if($pg == 'nilai'){
		include('kontent/nilai.php');	
	}   else if($pg == 'nilai_lihat'){
		include('kontent/nilai_lihat.php');	
	}   else if($pg == 'lihat_jadwal'){
		include('kontent/jadwal_lihat.php');	
	}    
	
	?>
</body>
</html>
<?php }else{
	?><script language="javascript">
	alert("Maaf, Anda Harus Login Dulu Untuk Mengakses Halaman Ini!!");
	document.location="login.php?page=login";
	</script>
	<?php 
} ?>