<?php
	global $pdo;
	$query = $pdo->prepare("SELECT login_guru.id_karyawan, karyawan.nama_lengkap, login_guru.status, login_guru.username FROM login_guru LEFT JOIN karyawan ON karyawan.id_karyawan = login_guru.id_karyawan_f WHERE login_guru.id_karyawan = ?");
	$query->bindValue(1, $id_user);
	$query->execute();	
	$baris = $query->fetch();
 ?>
<div class="content-kanan">
<div class="judul-branda-awal">Selamat Datang di Halaman Guru MTs NW Perian</div>
Selamat datang <b><?php echo $baris['nama_lengkap']; ?></b>, anda Log In dengan username <b><?php echo $baris['username']; ?></b> dan hak akses <b><?php echo $baris['status']; ?></b><br /><br />
<ul>
<li class="li-class"><b>Manajemen Berita</b><br>- Tulis, Edit, Hapus Berita di website MTs NW Perian</li>
<li class="li-class"><b>Pengumuman</b><br>- Tulis, Edit, Hapus pengumuman di website MTs NW Perian</li>
<li class="li-class"><b>Manajemen Prestasi & Penelitian Ustadz</b><br>- Tulis, Edit, Hapus Penelitian & Prestasi di website MTs NW Perian</li>
<li class="li-class"><b>Akademik</b><br>manajemen Aktifitas Akademik MTs NW Perian
<ul>
<li class="li-class"><b>Manajemen Nilai</b><br>manajemen Nilai Santri, Entry, hapus, edit</li>
<li class="li-class"><b>Manajemen Absensi</b><br>manajemen Absensi Siswa, Entry, Hapus, Edit</li>
</ul>
</li>
<li class="li-class"><b>Manajemen Galeri</b><br>manajemen Berita Foto/ Galery</li>
<li class="li-class"><b>Upload File</b><br>upload file ke website</li>
<li class="li-class"><b>Log Out</b><br>- Keluar dari control panel dan akhiri session</li>
</ul>
</div>