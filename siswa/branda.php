<?php
session_start();
include_once('../konek/coonnect.php');
include_once('../query/queryClass.php');

$qC = new queryClass;
$beritaawal = $qC->berita_awal();
$pengumuman = $qC->pengumuman_siswa();
$kegiatan = $qC->kegiatan();
$dakwah = $qC->dakwah();
$penghasilan = $qC->penghasilan_wali();
$agama = $qC->agama();
$pendidikan = $qC->pendidikan_terahir();
$pekerjaan_wali = $qC->pekerjaan_wali();
$tahun_ajaran = $qC->tahun_ajaran();
$smester = $qC->smester();
$mapel = $qC->mapel();

if(isset($_SESSION['id_siswa'])){
	$id_sis = $_SESSION['id_siswa'];
$pg = $_GET['page'];


?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $pg; ?> > santri mts nw perian</title>
    <link rel="shortcut icon" href="../gambar/favicon.png" />
	<meta name="author" content="Antonio Pratas">
	<link rel="stylesheet" type="text/css" href="asset/style.css">
	<link rel="stylesheet" type="text/css" href="asset/style2.css">
	<script src="../js/jquery2.1.js"></script>
    <script type="text/javascript" src="../tinymce/js/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
    selector: "textarea#isi_body",
    theme: "modern",
    width: 937,
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/Style.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 
</script>
	<script type="text/javascript">
		$(document).on("scroll",function(){
			if($(document).scrollTop()>20){ 
				$("header").removeClass("large").addClass("small");
				}
			else{
				$("header").removeClass("small").addClass("large");
				}
			});
	</script>

</head>

<body>

<header class="large">
		<?php include('template/nav.php'); ?>
	</header>

<section class="stretch">
	<div class="main">	
    <?php if($pg =='branda') {
		include('kontent/main-branda.php'); 
		}else if($pg == 'nilai'){
		include('kontent/main-sub.php');	
		}else if($pg == 'dakwah'){
		include('kontent/dakwah.php');	
		}else if($pg == 'biodata'){
		include('kontent/biodata.php');	
		}else if($pg == 'kelas'){
		include('kontent/kelas.php');	
		}
		
		?>
	</div>
    
</section>
	

</body>

</html>
<?php
}else{
	?><script language="javascript">
	alert("Maaf, Anda Harus Login Dulu Untuk Mengakses Halaman Ini!!");
	document.location="login.php?page=login";
	</script>
	<?php 	
	
}
?>