<div class="main-side">
<h2>Cari Nilai</h2>
<form method="post" action="?page=nilai&aksi=detail_us">
	<select name="k">
    	<option>KELAS</option>
        <?php $id_s = $id_sis;
$i_k = $qC->detail_kelas_s($id_s); 
 foreach($i_k as $k){ echo "<option value='".$k['ruang']."'>".$k['ruang']."</option>"; } ?>
    </select>
	<select name="t_a">
    	<option>TAHUN AJARAN</option>
         <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
	<br />
    <select name="s">
    	<option>SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
    <br />
    <input type="hidden" name="id_s" value="<?php echo $id_sis; ?>" />
    <input type="submit" name="cari" />
</form>
</div>
<?php if(isset($_GET['pilih'])){ ?>
<div class="main-content">
<div class="h_p">Lihat Nilai > Pilih Ulangan</div>
<a href="?page=nilai&aksi=us"><div class="tmb_kelas_siswa">LIHAT NILAI ULANGAN SMESTER</div></a><a href="?page=nilai&aksi=pr"><div class="tmb_kelas_siswa">LIHAT NILAI PEKERJAAN RUMAH</div></a><a href="?page=nilai&aksi=uh"><div class="tmb_kelas_siswa">LIHAT NILAI ULANGAN HARIAN</div></a>
</div>
<?php }else if($_GET['aksi']=='detail_us') { ?>
<div class="main-content">
<h1>NILAI UJIAN </h1>
<?php if(isset($_POST['k']) && ($_POST['t_a']) && ($_POST['id_s'])){
		$ruang_kel = $_POST['k'];$thun = $_POST['t_a'];$id_s = $_POST['id_s'];$s_m = $_POST['s'];
		$kelas_siswa = $qC->d_s_k3($ruang_kel, $thun, $id_s); ?>
         <div class="h_p">Nilai > Ulangan Smester Kelas <?php echo $ruang_kel; ?></div>
         <table class="lebar-table1">
		<tr><td>Nama</td><td> : </td><td><?php echo $kelas_siswa['nama_siswa']; ?></td><td width="30%"></td><td>Tahun Ajaran</td><td> : </td><td><?php echo $thun; ?></td></tr>
        <tr><td>NIP</td><td> : </td><td><?php echo $kelas_siswa['id_siswa']; ?></td><td width="30%"></td><td>Kelas</td><td> : </td><td><?php echo $ruang_kel; ?></td></tr>
        <tr><td>Smester</td><td> : </td><td><?php echo $s_m; ?></td><td width="30%"></td><td>Wali Kelas</td><td> : </td><td><?php echo $kelas_siswa['nama_lengkap']; ?></td></tr>
		</table>
        <br />
		<table class="tabel-raport" border="1">
        <tr><th width="5%">No</th><th>Mata Pelajaran</th><th>Nilai Hasil Belajar</th></tr>
		<?php 
			$no =1;
			$n = $qC->nilai_us2($ruang_kel, $thun, $id_s);	
			foreach($n as $ns){ ?>
			<tr><td><?php echo $no; ?></td><td><?php echo $ns['nama_mp']; ?></td><td><?php echo $ns['nilai_smester']; ?></td>
            </tr>
			<?php $no++;  }}?>
           </table>
</div>
<?php }else if($_GET['aksi']=='uh') { ?>
<div class="main-content">
<div class="h_p">Lihat Nilai > Nilai Ulangan Harian</div>
<form method="post" action="?page=nilai&aksi=detail_uh">

	<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
       <?php $id_s = $id_sis;
$i_k = $qC->detail_kelas_s($id_s); 
 foreach($i_k as $k){ echo "<option value='".$k['ruang']."'>".$k['ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
     <select name="mp" class="jdwl-frm">
    	<option value="">MAPEL</option>
          <?php foreach($mapel as $sm){ echo "<option value='".$sm['id_mp']."'>".$sm['nama_mp']."</option>"; } ?>
    </select>
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>

</div>

<?php } else if($_GET['aksi']=='pr') { ?>
<div class="main-content">
<div class="h_p">Lihat Nilai > Nilai Pekerjaan Rumah</div>
<form method="post" action="?page=nilai&aksi=detail_pr">
	<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
        <?php $id_s = $id_sis;
$i_k = $qC->detail_kelas_s($id_s); 
 foreach($i_k as $k){ echo "<option value='".$k['ruang']."'>".$k['ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
    <select name="mp" class="jdwl-frm">
    	<option value="">MAPEL</option>
          <?php foreach($mapel as $sm){ echo "<option value='".$sm['id_mp']."'>".$sm['nama_mp']."</option>"; } ?>
    </select>
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>
</div>
<?php } else if($_GET['aksi']=='us') { ?>
<div class="main-content">
<div class="h_p">Lihat Nilai > Nilai Ulangan Smester</div>
<form method="post" action="?page=nilai&aksi=detail_us">
<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
       <?php $id_s = $id_sis;
$i_k = $qC->detail_kelas_s($id_s); 
 foreach($i_k as $k){ echo "<option value='".$k['ruang']."'>".$k['ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
    <input type="hidden" name="id_s" value="<?php echo $id_sis; ?>" />
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>

</div>

<?php } ?>