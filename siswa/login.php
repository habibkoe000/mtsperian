<?php
session_start();

include_once('../konek/coonnect.php');

if(isset($_POST['masuk'])){$user = $_POST['user'];$password = md5($_POST['password']);
		if(empty($user) or empty($password)){$error = 'Maaf User atau Password Masih Kosong';
		}else{
			$query = $pdo->prepare("SELECT id_siswa FROM login_siswa WHERE username=? AND password=?");
			$query->bindValue(1, $user);$query->bindValue(2, $password);$query->execute();
			$data = $query->fetch();$num = $query->rowCount();
			if($num == 1){$_SESSION['id_siswa']=$data['id_siswa'];header('Location: branda.php?page=branda');exit();
			}else{$error='Maaf Kesalahan User Atau Password';}
		}		
}else if(isset($_POST['reg'])){
	$nis = $_POST['id_siswa'];
	$c_nis = $_POST['id_siswa_c'];
	$user_s = $_POST['user'];
	$pass_s = md5( $_POST['pass']);
	$status = $_POST['status'];
	if(empty($nis)||empty($c_nis)||empty($user_s)||empty ($pass_s)){ $error = 'Maaf, Jangan Biarkan Form Kosong'; }
	else{
		if($nis != $c_nis){$error = 'Maaf, NIS anda tidak sama, cek lagi';
		}else{
		$q = $pdo->prepare ("SELECT * from data_siswa where id_siswa=?");
			$q->bindValue(1, $c_nis);$q->execute();$num = $q->rowCount();
			if ($num > 0) { 
				$q2 = $pdo->prepare ("SELECT * from login_siswa where id_siswa = ? ");
				$q2->bindValue(1, $nis);$q2->execute();$num = $q2->rowCount();
				if ($num > 0) {$error ='Maaf, Anda Sudah Mendaftar Sebelumnya';
				}else{
					$query = $pdo->prepare('INSERT INTO login_siswa (id_siswa, username, password, status, id_siswa_f) VALUES(?,?,?,?,?)');
					$query->bindValue(1, $nis);
					$query->bindValue(2, $user_s);
					$query->bindValue(3, $pass_s);
					$query->bindValue(4, $status);
					$query->bindValue(5, $c_nis);		
					$query->execute();
					if($query){ $error = 'Anda Berhasil Mendaftar, Selamat Datang Di MTs NW Perian, Silahkan <a href="?page=login">Login Di Sini</a>';	}
				}
			}else{ $error ='Anda Bukan Siswa MTs NW Perian, atau NIS anda tidak terdaftar, Hubungi Guru, Terimakasih!!'; }
		}
	}
	
}

$pg = $_GET['page'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pg; ?> Santri</title>
<link rel="shortcut icon" href="../gambar/favicon.png" />
<link rel="stylesheet" href="asset/login-css.css" type="text/css" />
</head>

<body class="bglogin">

<div class="login">
<?php if ($pg == 'login'){ ?>
	<div class="head-login"></div>
    <div class="screen-login"><img src="gambar/menubg.png" /><?php if (isset($error)) { ?><div class="screen-pesan"><?php echo $error; ?></div><?php }?>
    </div>
    <div class="menu-login">
    <form  method="post" autocomplete="off">
        <table>
        <tr><td colspan="2"><input type="text" name="user" autofocus="autofocus" placeholder="Username" class="form-login" /></td></tr>
        <tr><td colspan="2"><input type="password" name="password" placeholder="Password" class="form-login" /></td></tr>
        <tr><td><input type="submit" name="masuk" value="Masuk" class="tombol-login" /></td><td><input type="reset" name="reset" value="Reset" class="tombol-login" /></td></tr>
        </table>
        </form>
    </div>
	<div class="footer-login"> <a href="http://localhost/mtsnwperian.esy.es">Official Site</a> | <a href="?page=register">Daftar</a>
    </div>
    <?php } else if($pg == 'register') { ?>
    <div class="head-reg"></div>
     <div class="screen-reg">
    <?php if (isset($error)) { ?><div class="screen-pesan"><?php echo $error; ?></div><?php } else { echo 'yang bisa mendaftar di layanan ini hanya yang berstatus Siswa di MTs NW Perian, dan telah memiliki NIS, bila telah memiliki NIS tapi belum bisa Daftar, Hubungi Guru terimakasih!! '; }?>
    </div>
    <div class="menu-login">
    <form  method="post" autocomplete="off">
    <input type="hidden" name="status" value="santri" />
        <table>
         <tr><td colspan="2"><input type="text" name="id_siswa" autofocus="autofocus" placeholder="id Siswa atau NIS" class="form-login" /></td></tr>
          <tr><td colspan="2"><input type="text" name="id_siswa_c" placeholder="ulang tulis id Siswa atau NIS" class="form-login" /></td></tr>
        <tr><td colspan="2"><input type="text" name="user" placeholder="Username" class="form-login" /></td></tr>
        <tr><td colspan="2"><input type="password" name="pass" placeholder="Password" class="form-login" /></td></tr>
        <tr><td><input type="submit" name="reg" value="Daftar" class="tombol-login" /></td><td><input type="reset" name="reset" value="Reset" class="tombol-login" /></td></tr>
        </table>
        
        </form>
    </div>
	<div class="footer-login"> <a href="http://localhost/mtsnwperian.esy.es">Official Site</a> | <a href="?page=login">Masuk</a>
    </div>
    <?php } ?>
</div>
</body>
</html>