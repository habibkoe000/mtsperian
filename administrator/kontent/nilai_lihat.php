<?php if($_GET['aksi']=='lihat_nilai') {  ?>
<div class="content-kanan">
<div class="h_p">Lihat Nilai > Pilih Ulangan</div>
<a href="?page=nilai_lihat&aksi=us"><div class="tmb_kelas_siswa">LIHAT NILAI ULANGAN SMESTER</div></a><a href="?page=nilai_lihat&aksi=pr"><div class="tmb_kelas_siswa">LIHAT NILAI PEKERJAAN RUMAH</div></a><a href="?page=nilai_lihat&aksi=uh"><div class="tmb_kelas_siswa">LIHAT NILAI ULANGAN HARIAN</div></a>
</div>
<?php }else if($_GET['aksi']=='uh') { ?>
<div class="content-kanan">
<div class="h_p">Lihat Nilai > Nilai Ulangan Harian</div>
<form method="post" action="?page=nilai_lihat&aksi=detail_uh">

	<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
        <?php foreach($siswa_kelas as $sk){ echo "<option value='".$sk['nama_ruang']."'>".$sk['nama_ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
     <select name="mp" class="jdwl-frm">
    	<option value="">MAPEL</option>
        <?php foreach($mapel as $sm){ echo "<option value='".$sm['id_mp']."'>".$sm['nama_mp']."</option>"; } ?>
    </select>
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>

</div>

<?php } else if($_GET['aksi']=='pr') { ?>
<div class="content-kanan">
<div class="h_p">Lihat Nilai > Nilai Pekerjaan Rumah</div>
<form method="post" action="?page=nilai_lihat&aksi=detail_pr">
	<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
        <?php foreach($siswa_kelas as $sk){ echo "<option value='".$sk['nama_ruang']."'>".$sk['nama_ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
    <select name="mp" class="jdwl-frm">
    	<option value="">MAPEL</option>
        <?php foreach($mapel as $sm){ echo "<option value='".$sm['id_mp']."'>".$sm['nama_mp']."</option>"; } ?>
    </select>
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>
</div>
<?php } else if($_GET['aksi']=='us') { ?>
<div class="content-kanan">
<div class="h_p">Lihat Nilai > Nilai Ulangan Smester</div>
<form method="post" action="?page=nilai_lihat&aksi=detail_us">
<select name="t_a" class="jdwl-frm">
    	<option value="">TAHUN AJARAN</option>
        <?php foreach($tahun_ajaran as $k){ echo "<option value='".$k['tahun']."'>".$k['tahun']."</option>"; } ?>
    </select>
    <select name="k" class="jdwl-frm">
    	<option value="">PILIH KELAS</option>
        <?php foreach($siswa_kelas as $sk){ echo "<option value='".$sk['nama_ruang']."'>".$sk['nama_ruang']."</option>"; } ?>
    </select>
    <select name="s" class="jdwl-frm">
    	<option value="">SMESTER</option>
        <?php foreach($smester as $sm){ echo "<option value='".$sm['id_smester']."'>".$sm['nama_smester']."</option>"; } ?>
    </select>
	<input type="submit" name="simpan" value="Lihat Nilai"  class="jdwl-btn"/>
</form>

</div>

<?php } else if($_GET['aksi']=='detail_us') { ?>
<div class="content-kanan">

  <?php if(isset($_POST['k']) && ($_POST['t_a'])){
		$ruang = $_POST['k'];$thun = $_POST['t_a'];$s_m = $_POST['s'];
		$kelas_siswa = $dataClass->detail_siswa_kelas($ruang, $thun); ?>
         <div class="h_p">Nilai > Ulangan Smester Kelas <?php echo $ruang; ?></div>
         <table class="lebar-table1">
		<?php 
		echo 'Smester '.$s_m.' Tahun Ajaran '.$thun;
		echo '<tr class="tbl2 thl"><th>NO</th><th>NIP</th><th>NAMA SISWA</th><th>INPUT NILAI</th></tr>';
		$no =1;
		foreach($kelas_siswa as $ks){ ?>
		<tr><td><?php echo $no; ?></td><td><?php echo $ks['id_siswa']; ?></td><td><?php echo $ks['nama_siswa']; ?></td>
        <td><a href="?page=nilai_lihat&aksi=nilai_us&k=<?php echo $ruang; ?>&t_a=<?php echo $thun; ?>&id_s=<?php echo $ks['id_siswa'];?>&s=<?php echo $s_m; ?>" target="_blank">LIHAT NILAI</a></td></tr>
 <?php $no++; } ?>
 </table>
<?php } ?>
</div>
<?php } else if($_GET['aksi']=='detail_uh') { ?>
	<div class="content-kanan">
  <?php if(isset($_POST['k']) && ($_POST['t_a'])){
		$ruang = $_POST['k'];$thun = $_POST['t_a'];$s_m = $_POST['s'];$mp = $_POST['mp'];
		$kelas_siswa = $dataClass->nilai_uh($ruang, $thun); ?>
         <div class="h_p">Nilai > Ulangan Harian Kelas <?php echo $ruang; ?></div>
         <table class="tabel-raport" border="1">
         <tr><th>RUANG KELAS</th><th>SMESTER</th><th>TAHUN AJARAN</th><th>MAPEL</th></tr>
         <tr><td><?php echo $ruang; ?></td><td><?php echo $s_m; ?></td><td><?php echo $thun; ?></td><td><?php echo $mp; ?></td></tr>
         </table>
         <br />
         <table class="tabel-raport" border="1">
		<?php 
		echo '<tr><th>NO</th><th>NIP</th><th>NAMA SISWA</th><th>NILAI</th><th colspan="2" width="20%">PENGATURAN</th></tr>';
		$no =1;
		foreach($kelas_siswa as $ks){ ?>
		<tr><td><?php echo $no; ?></td><td><?php echo $ks['id_siswa']; ?></td><td><?php echo $ks['nama_siswa']; ?></td><td><?php echo $ks['nilai_UH']; ?></td><td>EDIT</td><td>HAPUS</td></tr>
        <?php $no++; } ?>
</table><?php } ?>
</div>
<?php } else if($_GET['aksi']=='detail_pr') { ?>
<div class="content-kanan">
  <?php if(isset($_POST['k']) && ($_POST['t_a'])){
		$ruang = $_POST['k'];$thun = $_POST['t_a'];$s_m = $_POST['s'];$mp = $_POST['mp'];
		$kelas_siswa = $dataClass->nilai_pr($ruang, $thun); ?>
         <div class="h_p">Nilai > Pekerjaan Rumah Kelas <?php echo $ruang; ?></div>
         <table class="tabel-raport" border="1">
         <tr><th>RUANG KELAS</th><th>SMESTER</th><th>TAHUN AJARAN</th><th>MAPEL</th></tr>
         <tr><td><?php echo $ruang; ?></td><td><?php echo $s_m; ?></td><td><?php echo $thun; ?></td><td><?php echo $mp; ?></td></tr>
         </table>
         <br />
         <table class="tabel-raport" border="1">
		<?php 
		echo '<tr><th>NO</th><th>NIP</th><th>NAMA SISWA</th><th>NILAI</th><th colspan="2" width="20%">PENGATURAN</th></tr>';
		$no =1;
		foreach($kelas_siswa as $ks){ ?>
		<tr><td><?php echo $no; ?></td><td><?php echo $ks['id_siswa']; ?></td><td><?php echo $ks['nama_siswa']; ?></td><td><?php echo $ks['nilai_PR']; ?></td><td>EDIT</td><td>HAPUS</td></tr>
        <?php $no++; } ?>
</table>
	 <?php } ?>
    </div>
<?php } else if($_GET['aksi']=='nilai_us') { ?>
<div class="content-kanan">
<?php if(isset($_GET['k']) && ($_GET['t_a']) && ($_GET['id_s'])){
		$ruang_kel = $_GET['k'];$thun = $_GET['t_a'];$id_s = $_GET['id_s'];$s_m = $_GET['s'];
		$kelas_siswa = $dataClass->d_s_k3($ruang_kel, $thun, $id_s); ?>
         <div class="h_p">Nilai > Ulangan Smester Kelas <?php echo $ruang_kel; ?></div>
         <table class="lebar-table1">
		<tr><td>Nama</td><td> : </td><td><?php echo $kelas_siswa['nama_siswa']; ?></td><td width="30%"></td><td>Tahun Ajaran</td><td> : </td><td><?php echo $thun; ?></td></tr>
        <tr><td>NIP</td><td> : </td><td><?php echo $kelas_siswa['id_siswa']; ?></td><td width="30%"></td><td>Kelas</td><td> : </td><td><?php echo $ruang_kel; ?></td></tr>
        <tr><td>Smester</td><td> : </td><td><?php echo $s_m; ?></td><td width="30%"></td><td>Wali Kelas</td><td> : </td><td><?php echo $kelas_siswa['nama_lengkap']; ?></td></tr>
		</table>
        <br />
		<table class="tabel-raport" border="1">
        <tr><th width="5%">No</th><th>Mata Pelajaran</th><th>Nilai Hasil Belajar</th><th colspan="2" width="20%">Pengaturan</th></tr>
		<?php 
			$no =1;
			$n = $dataClass->nilai_us2($ruang_kel, $thun, $id_s);	
			foreach($n as $ns){ ?>
			<tr><td><?php echo $no; ?></td><td><?php echo $ns['nama_mp']; ?></td><td><?php echo $ns['nilai_smester']; ?></td><td>EDIT</td><td>HAPUS</td></tr>
			<?php $no++;  }}?>
           </table>
</div>
<?php } ?>