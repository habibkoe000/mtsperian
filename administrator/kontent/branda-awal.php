<?php
	global $pdo;$query = $pdo->prepare("SELECT nama, status, ussername FROM user WHERE id = ?");
	$query->bindValue(1, $id_user);$query->execute();$baris = $query->fetch(); ?>
<div class="content-kanan">
<div class="judul-branda-awal">Selamat Datang di Halaman Administrator MTs NW Perian</div>
Selamat datang <b><?php echo $baris['nama']; ?></b>, anda Log In dengan username <b><?php echo $baris['ussername']; ?></b> dan hak akses <b><?php echo $baris['status']; ?></b><br /><br />
<ul>
<li class="li-class"><b>Manajemen Berita</b><br>- Tulis, Edit, Hapus Berita di website MTs NW Perian</li>
<li class="li-class"><b>Pengumuman</b><br>- Tulis, Edit, Hapus pengumuman di website MTs NW Perian</li>
<ul>
<li class="li-class"><b>Agenda Madrasah</b><br>- Tulis, Edit, Hapus Agenda Kegiatan di website MTs NW Perian</li>
<li class="li-class"><b>Manajemen Prestasi & Penelitian Ustadz</b><br>- Tulis, Edit, Hapus Penelitian & Prestasi di website MTs NW Perian</li>
<li class="li-class"><b>Manajemen Profil Madrasah</b><br>- Tulis, Edit, Hapus Profil Madrasah di website MTs NW Perian</li>
</ul>
<li class="li-class"><b>Akademik</b><br>manajemen Aktifitas Akademik MTs NW Perian
<ul>
<li class="li-class"><b>Manajemen Nilai</b><br>manajemen Nilai Santri, Entry, hapus, edit</li>
<li class="li-class"><b>Manajemen Mata Pelajaran</b><br>Mengatur Mata Pelajaran santri</li>
<li class="li-class"><b>Manajemen Jadwal Belajar</b><br>manajemen Jadwal Belajar santri,Entry, Hapus, Edit</li>
<li class="li-class"><b>Manajemen Absensi</b><br>manajemen Absensi Siswa, Entry, Hapus, Edit</li>
</ul>
</li>
<li class="li-class"><b>Manajemen Kelas</b><br>manajemen Data Kelas Santri</li>
<li class="li-class"><b>Data Umum</b><br>manajemen Data yang bersifat Umum
<ul>
<li class="li-class"><b>Manajemen Agama</b><br>manajemen Agama, Entry, Hapus, Edit</li>
<li class="li-class"><b>Manajemen Pekerjaan</b><br>manajemen Pekerjaan</li>
<li class="li-class"><b>Manajemen Status Pendidikan</b><br>manajemen Pendidikan, sebagai data pendidikan terahir untuk wali, atau guru dan pegawai madrasah</li>
<li class="li-class"><b>Manajemen Penghasilan</b><br>manajemen Penghasilan perbulan</li>
</ul>
</li>

<li class="li-class"><b>Data Santri-Santriwati</b><br>manajemen Data Santri, Entry, Hapus, Edit</li>
<li class="li-class"><b>Data Ustadz & Pegawai</b><br>manajemen Data Pegawai, Entry, Hapus, Edit</li>
<li class="li-class"><b>Manajemen Fasilitas Madrasah</b><br>manajemen Data Fasilitas Madrasah</li>

</li>
<li class="li-class"><b>Manajemen Galeri</b><br>manajemen Berita Foto/ Galery</li>
<li class="li-class"><b>Upload File</b><br>upload file ke website</li>
<li class="li-class"><b>Log Out</b><br>- Keluar dari control panel dan akhiri session</li>
</ul>
</div>