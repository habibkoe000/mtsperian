 <div class="content">
        <div class="head_right">
            <div class="head_right_isi">
        		<div id="tuts-slider">	 
                    <ul class="bjqs">
                    <?php 
					foreach($berita_slide as $bs) { ?>
                        <li class="bjqs-slide clone">
                        	<a href="branda.php?page=detail_info&id_b=<?php echo $bs['id_berita'];?>">
                            <img src="gambar_berita/<?php echo $bs['foto'];?>" alt="<?php echo $bs['judul'];?>" title="<?php echo $bs['judul'];?>"></a></li>			
                        <?php } ?>			
                    </ul>		
				</div>
        	</div>
        </div>
        <div class="content_right">
        	<div class="berita_utama">
            <div class="ayat_hari_ini">
            	<div class="judul_ayat">
                	Al-Qur'an
                </div>
            	<div class="ayat_hari_ini_isi">
                	<marquee scrollamount="2">Dengan Menyebut Nama Allah Yang Maha Pengasih Lagi Maha Penyayang</marquee>
                </div>
            
            </div>
            <?php foreach($beritaawal as $ba) {  ?>
            <a href="branda.php?page=detail_info&id_b=<?php echo $ba['id_berita'];?>"><div class="berita_utama_isi">
            <img src="gambar_berita/<?php echo $ba['foto'];?>" height="105" width="150" class="img_detail" />
            <div class="tgl"><?php echo date('j | F, Y | g:i a', strtotime($ba['tanggal'])); ?> </div>
            <div class="judul_detail_berita"><?php echo $ba['judul']; ?></div></div></a>
            <?php } ?>
            </div>
            <div class="sidebar_right">
            
            	<div class="sidebar_right_isi">
           			<div id="newsticker-demo">    
  						<a href="branda.php?page=semua_info&all=kegiatan"><div class="title">AGENDA KEGIATAN</div></a>
   						 <div class="newsticker-jcarousellite">
							<ul>
       							 <?php
									foreach($kegiatan as $k){ ?>
          								 <a href="branda.php?page=detail_info&id_k=<?php echo $k ['id_kegiatan'];?>"> <li>
										<div class="thumbnail">
										<img src="gambar/agenda3.png" />
										</div>
										<div class="info">
                                        <span class="cat"><?php echo 'post: '.date('j | F, Y | g:i a', strtotime($k['tanggal_posting']));?></span>
										 <a href="branda.php?page=detail_info&id_k=<?php echo $k ['id_kegiatan'];?>"><?php echo $k['tema_kegiatan']; ?></a>
										
										</div>
										<div class="clear"></div>
										</li></a>
								<?php } ?>
       						 </ul>
   						 </div>
					</div>
            	</div>
                <div class="sidebar_right_isi">
           			<a href="branda.php?page=semua_info&all=pengumuman"><div class="judul_side">PENGUMUMAN</div></a>
                    <div class="right_side_isi">
                    <?php
                    foreach($pengumuman as $p){ ?>
                    <a href="branda.php?page=detail_info&id_p=<?php echo $p ['id_pengumuman'];?>"><li><?php echo $p['judul']; ?></li></a>
                    <?php } ?>
                    </div>
            	</div>
                
                <div class="sidebar_right_isi">
                <div class="judul_side"> DAKWAH SISWA </div>
                	 <div class="right_side_isi">
                   		<?php foreach ($dakwah as $d){
						$isilimit = substr($d['isi'],0,200);
	 			echo  $isilimit;
				} ?>
                    </div>
                </div>
            </div>
    	</div>
    </div>
    