<div class="dala-menu-kiri">
        <div id="wrapper">

	<ul class="menu">
		<li class="item6"><a href="#">Pengumuman</a>
			<ul>
				<li class="subitem1"><a href="?page=pengumuman&aksi=opsi">Tambah Pengumuman</a></li>
				<li class="subitem2"><a href="?page=pengumuman&aksi=lihat">Lihat Pengumuman</a></li>
			</ul>
		</li>
        <li class="item7"><a href="#">Prestasi & Penelitian</a>
        	<ul>
                 <li class="subitem2"><a href="#">Penelitian Guru</a></li>
            	<li class="subitem3"><a href="#">Tambah Prestasi Guru</a></li>
				<li class="subitem4"><a href="#">Lihat Prestasi Guru</a></li>
            </ul>
		</li>
        <li class="item11"><a href="#">Akademik</a>
        	<ul>
            	<li class="subitem3"><a href="?page=nilai&aksi=tambah">Tambah Nilai Siswa</a></li>
				<li class="subitem4"><a href="?page=nilai_lihat&aksi=lihat_nilai">Lihat Nilai Siswa</a></li>
                <li class="subitem5"><a href="?page=absensi&aksi=lihat">Absensi Siswa</a></li>
            </ul>
		</li>
         <li class="item1"><a href="#">Galery</a>
		</li>
         <li class="item14"><a href="#">Upload</a>
		</li>
	</ul>

</div>

<!--initiate accordion-->
<script type="text/javascript">
	$(function() {
	
	    var menu_ul = $('.menu > li > ul'),
	           menu_a  = $('.menu > li > a');
	    
	    menu_ul.hide();
	
	    menu_a.click(function(e) {
	        //e.preventDefault();
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	
	});
</script>
   
   	</div>
   <div class="footer-menu-kiri">
    Copyrigh &copy; MTs NW Perian 2014
    </div>
