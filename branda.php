<?php

include('query/queryClass.php');
include('konek/coonnect.php');

$qC = new queryClass;
$beritaawal = $qC->berita_awal();
$pengumuman = $qC->pengumuman();
$kegiatan = $qC->kegiatan();
$semua_kegiatan = $qC->semua_kegiatan();
$semua_pengumuman = $qC->semua_pengumuman();
$profil_Madrasah = $qC->profil_sekolah();
$fasilitas = $qC->fasilitas();
$berita_slide = $qC->berita_awal_slide();
$dakwah = $qC->dakwah();
$guru = $qC->guru();
$pg = $_GET['page'];

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta name="description" content="madrasah tsanawiyah nw perian">
<meta name="keywords" content="mts nw perian, madrasah tsanawiyah nw perian, yp3dm, yayasan pendidikan pondok pesantren nw perian, ponpes perian, ponpes nw perian, mts perian">
<meta name="author" content="muhammad habiburrahman, habib koe tkj, bee ipid">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pg; ?> > mts nw perian</title>
<link rel="shortcut icon" href="gambar/favicon.png" />
<link rel="stylesheet" href="asset/ticker.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="asset/style.css" type="text/css"/>
<link rel="stylesheet" href="asset/slider.css" type="text/css" />
<link rel="stylesheet" href="asset/css_flip.css" type="text/css" />
<link rel="stylesheet" href="asset/bootstrap.css" type="text/css" />
<script type="text/javascript" src="js/jquery2.1.js"></script>
<script src="js/jcarousellite_1.0.1c4.js" type="text/javascript"></script>
<script type="text/javascript" src="js/basic-jquery-slider.js"></script>
<script>
jQuery(document).ready(function() {
    	jQuery('#tuts-slider').bjqs({
		'animation' : 'slide',
		'width' : 660,
		'height' : 340,
		'showMarkers' :false,

	 });
});
</script>

<script type="text/javascript">
$(function() {
	$(".newsticker-jcarousellite").jCarouselLite({
		vertical: true,
		hoverPause:true,
		visible: 1,
		auto:6000,
		speed:300,
	});
	});
</script>

</head>

<body>
<div class="main">
	<?php
		include('template/sidebar.php');
		
		if($pg == 'branda'){
		include('kontent/content_home.php');
		}else if($pg=='semua_info'){
		include 'kontent/semua_info.php';
		}else if($pg=='detail_info'){
		include 'kontent/detail_info.php';
		}
		include('template/footer.php');
	?>
   
</div>
 <div class="bottom-news">
 <a href="index.php"><img src="gambar/favicon.png" width="30" height="30" /></a>
<marquee>Assalamualaikum Warohmatullohi Wabarokatuh, Ahlan Wasahlan , Selamat Datang di Situs Resmi Madrasah Tsanawiyah NW Perian
</marquee></div>

</body>
</html>